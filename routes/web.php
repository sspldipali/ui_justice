<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});
//Home Routes
Route::get('home', 'DashboardController@index');

//Login Routes
Route::post('/login', 'AuthController@getAuthenticationToken');

//Session Expired
Route::get('/sessionexpired', "AuthController@sessionExpired");

Route::post('/delete_user{id}', 'AuthController@delete_user');

//authentication Routes
Route::post('register', 'AuthController@register');
Route::get('login', 'AuthController@login_view');
Route::get('forgot_password', 'AuthController@forgot_password');
Route::get('reset_password/{id}', 'AuthController@reset_password');
Route::get('/', 'AuthController@login_view');

// Report Route
Route::get('send_report', 'reportsController@send_report');
Route::get('delivery_history', 'reportsController@delivery_history');

//Logout Routes
Route::get('/logout', 'AuthController@get_logout');

Route::post('/submit/report', 'reportsController@submitReport');

//middleware Routes
Route::group(["middleware" => 'dsession'], function () {

    //system adminstration Routes
    Route::get('/manage_user', 'SystemadministratorController@get_manage_user');
    Route::get('access_report', 'SystemadministratorController@get_access_log');
    Route::get('/error_log', 'SystemadministratorController@get_error_log');

    //Reports( All Charts)
    //Reports conroller Routes including courts
    Route::get('pie_in', 'reportsController@pie_in');
    Route::get('line_in', 'reportsController@line_in');
    Route::get('bar_in', 'reportsController@bar_in');

    //Reports controller Routes within courts
    Route::get('pie_wi', 'reportsController@pie_wi');
    Route::get('line_wi', 'reportsController@line_wi');
    Route::get('bar_wi', 'reportsController@bar_wi');
});
