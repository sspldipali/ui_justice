@include('common.header')
@include('common.sidebar')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
<link href="{!! asset('css/send_report_page.css') !!}" rel="stylesheet">
<style>
  .form_control_2 {
    margin-left: -44px;
    width: 105%;
  }

  .form_control_3 {
    margin-left: -16px;
    width: 105%;
  }

  .custom_file {
    width: 110%;
  }

  .margin_left_28 {
    margin-left: 28px;
  }
</style>
<script>
  var ctrlURL = "{{ url('/') }}";
</script>

<body class="app sidebar-fixed aside-menu-fixed sidebar-lg-show">
  <main class="main margin_top_66 margin_top_70">
    <header class="app-header navbar header_bk_co">
      <div class="whitecolor page_name">
        <h5 class="whitecolor"><a href="#" class=" text_font22 whitecolor">{{ trans('messages.Send_Report') }}</h5>
      </div>
    </header>
    <div class="container-fluid margin_top_35">
      <div class="animated fadeIn">
        <div class="row ">
          <div class="col-sm-12">
            <h5 class="whitecolor font_size_15 display_flex"><a href="#" class="whitecolor">{{ trans('messages.Home') }}</a> /{{ trans('messages.Reports1') }} </h5>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <form class="form-horizontal" action="{{url('/submit/report')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group padding_20">
                  @if (isset($message))
                  <div class="row">
                    <div class="col-lg-12" style="color: #155724; font-weight: 14px;">
                      {{ $message }}
                    </div>
                  </div>
                  @endif
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="row">
                            <label class="control-label col-sm-3" for="text">{{ trans('messages.Year') }}</label>
                            <div class="col-sm-7">
                              <input type="text" name="finYear" id="finYear" class="form-control form-control1" placeholder="{{ trans('messages.Year') }}">
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-5 mobliev_margin_top_15">
                          <div class="row">
                            <label class="control-label col-sm-4" for="text">{{ trans('messages.Quarter') }}</label>
                            <div class="col-sm-7">
                              <select name="quarter" id="quarter" class="form-control form-control1 margin_left_m_15 margin_left_0">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-1 mobliev_margin_top_15">
                          <div class="row">
                            <button type="button" name="btnPreview" id="btnPreview" class="btn btn-primary">{{ trans('messages.Preview_Button_Text') }}</button>
                          </div>
                        </div>
                      </div>
                      <div class="row  margin_top_15 margin_top_0">
                        <div class="col-sm-6">
                          <div class="row">
                            <label class="control-label col-sm-3" for="text">{{ trans('messages.Time') }}</label>
                            <div class="col-sm-7">
                              <input type="text" name="time" id="datetime" readonly="readOnly" style="background-color: transparent" class="form-control form-control1">
                              <!-- span class="form-control form-control1" for="time" id="datetime"></span -->
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6 mobliev_margin_top_15">
                          <div class="row">
                            <label class="control-label col-sm-4" for="text">{{ trans('messages.Notice') }}</label>
                            <div class="col-sm-7">
                              <input type="file" class="custom-file-input" id="customFile" name="noticeFile">
                              <label class="custom-file-label custom_file" for="customFile">{{ trans('messages.Choose_File') }}</label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row  margin_top_15 margin_top_0">
                        <div class="col-sm-6">
                          <div class="row">
                            <label class="control-label col-sm-3" for="text">{{ trans('messages.Letter No.') }}</label>
                            <div class="col-sm-7">
                              <input type="text" class="form-control form-control1" name="letterNo" placeholder="{{ trans('messages.Letter No.') }}">
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6 mobliev_margin_top_15">
                          <div class="row">
                            <label class="control-label col-sm-3" for="text">{{ trans('messages.Letter Date') }}</label>
                            <div class="col-sm-7">
                              <input type="date" class="form-control form-control1 margin_left_28" name="letterDate" placeholder="{{ trans('messages.Letter Date') }}">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="row margin_top_15 margin_top_0">
                        <label class="control-label col-sm-2" for="text">{{ trans('messages.Total_Number_Of_Cases') }}</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control form_control_2" name="noOfCases">
                        </div>

                        <label class="control-label col-sm-2" for="text">{{ trans('messages.Total_Amount_Of_Receipt') }}</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control form_control_3" name="receiptNumber">
                        </div>
                      </div>
                    </div>
                    <div class="row margin_top_15 margin_top_0" style="margin-top: 35px;">
                      <div class="col-sm-6"></div>
                      <div class="col-sm-6">
                        <button type="submit" style="margin-left: 440px;margin-top: -15px;" class="btn btn-primary upload_btn margin_top_15 margin_right_0">{{ trans('messages.Upload') }}</button>
                      </div>
                    </div>

                  </div>
                </div>
                <script>
                  var dt = new Date().toLocaleTimeString();
                  document.getElementById("datetime").value = dt.toLocaleString();
                  /*document.getElementById("datetime").innerHTML = dt.toLocaleString();*/
                </script>
              </form>
              <div class="card-body">
                <div class="border_cord">
                  <p class="col-sm-12 datadiv padding_left_right_20 whitecolor">{{ trans('messages.Case_List') }}</p>
                  <div class="col-sm-12 padding_left_right_10">
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered">
                        <thead>
                          <tr class="tr_bk_color">
                            <th>{{ trans('messages.Year') }}</th>
                            <th>{{ trans('messages.Quarter') }}</th>
                            <th>{{ trans('messages.Court') }}</th>
                            <th>{{ trans('messages.Black_Case') }}</th>
                            <th>{{ trans('messages.Name_Of_Debtor') }}</th>
                            <th>{{ trans('messages.Debt_Amount') }}</th>
                            <th>{{ trans('messages.Investigation_And_Enforcement') }}</th>
                          </tr>
                        </thead>
                        <tbody>
                          <!-- tr>
                            <td>1</td>
                            <td>district</td>
                            <td>Nonthaburi</td>
                            <td>Notifications 4/2018</td>
                            <td>Sommai Chaichan</td>
                            <td>20,000.00</td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>2</td>
                            <td>district</td>
                            <td>Nonthaburi</td>
                            <td>Notice of 1/2019</td>
                            <td>Sompong Chokchai</td>
                            <td>20,000.00</td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>3</td>
                            <td>district</td>
                            <td>Nonthaburi</td>
                            <td>Notifications 2/2019</td>
                            <td>Victory</td>
                            <td>20,000.00</td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>4</td>
                            <td>district</td>
                            <td>Saraburi</td>
                            <td>Notifications 3/2019</td>
                            <td>Somkid is like.</td>
                            <td>20,000.00</td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>5</td>
                            <td>district</td>
                            <td>Saraburi</td>
                            <td>Notifications 3/2019</td>
                            <td>Somrak Choengchai</td>
                            <td>20,000.00</td>
                            <td></td>
                          </tr -->
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="border_cord">
                  <p class="col-sm-12 datadiv whitecolor">{{ trans('messages.Recipt_List') }}</p>
                  <div class="col-sm-12 padding_left_right_10">
                    <div class="table-responsive">
                      <table id="example1" class="table table-striped table-bordered">
                        <thead>
                          <tr class="tr_bk_color">
                            <th>{{ trans('messages.Year') }}</th>
                            <th>{{ trans('messages.Quarter') }}</th>
                            <th>{{ trans('messages.Court') }}</th>
                            <th>{{ trans('messages.File_Name') }}</th>
                          </tr>
                        </thead>
                        <tbody>
                         <!-- <tr>
                            <td>1</td>
                            <td>district</td>
                            <td>Nonthaburi</td>
                            <td>110001101110011.pdf <button type="button" class="btn btn-secondary margin_left_10">{{ trans('messages.Download') }}</button></td>
                          </tr>
                          <tr>
                            <td>2</td>
                            <td>district</td>
                            <td>Nonthaburi</td>
                            <td>110001101110011.pdf <button type="button" class="btn btn-secondary margin_left_10">{{ trans('messages.Download') }}</button></td>
                          </tr>
                          <tr>
                            <td>3</td>
                            <td>district</td>
                            <td>Nonthaburi</td>
                            <td>110001101110011.pdf <button type="button" class="btn btn-secondary margin_left_10">{{ trans('messages.Download') }}</button></td>
                          </tr>
                          <tr>
                            <td>4</td>
                            <td>district</td>
                            <td>Saraburi</td>
                            <td>110001101110011.pdf <button type="button" class="btn btn-secondary margin_left_10">{{ trans('messages.Download') }}</button></td>
                          </tr>
                          <tr>
                            <td>5</td>
                            <td>district</td>
                            <td>Saraburi</td>
                            <td>110001101110011.pdf <button type="button" class="btn btn-secondary margin_left_10">{{ trans('messages.Download') }}</button></td>
                          </tr>-->
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    </div>
  </main>
  <!-- CoreUI and necessary plugins-->
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
  <script src="{!! asset('vendors/popper.js/js/popper.min.js') !!}" type="text/javascript"></script>
  <script src="{!! asset('vendors/bootstrap/js/bootstrap.min.js') !!}" type="text/javascript"></script>
  <script src="{!! asset('vendors/pace-progress/js/pace.min.js') !!}" type="text/javascript"></script>
  <script src="{!! asset('vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') !!}" type="text/javascript"></script>
  <script src="{!! asset('vendors/@coreui/coreui/js/coreui.min.js') !!}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      $('.navbar-light .dmenu').hover(function() {
        $(this).find('.sm-menu').first().stop(true, true).slideDown(150);
      }, function() {
        $(this).find('.sm-menu').first().stop(true, true).slideUp(105)
      });
    });
    $(document).ready(function() {
      $('#example').DataTable();
    });
    $(document).ready(function() {
      $('#example1').DataTable();
    });

    $("#btnPreview").click(function() {
      var finYear = $("#finYear").val();
      var quarter = $("#quarter").val();
      var reqData = {
        "finYear": finYear,
        "quarter": quarter
      };
      console.log("reqData: ", reqData);
      $.ajax({
        url: ctrlURL + "/api/report/preview",
        method: "POST",
        data: reqData,
        headers: {
          "Authorization": "Bearer " + token
        },
        type: "json",
        success: function(response) {
          $('#example').DataTable().destroy();
          response.data.caseList.forEach((previewRecord, index) => {
            $("#example tbody").append(
              `
              <tr>
                <td>${previewRecord.Year}</td>
                <td>${previewRecord.Quarter}</td>
                <td>${previewRecord.Court}</td>
                <td>${previewRecord.BlackNumber}</td>
                <td>${previewRecord.Debtor}</td>
                <td>${previewRecord.Debt}</td>
                <td>${previewRecord.Result}</td>
              </tr>
              `
            )
          });
		  
          $('#example').DataTable();
		  console.log(response.data.receiptList);
		  response.data.receiptList.forEach((previewRecord1, index) => {
			  console.log("previewRecord1: ", previewRecord1);
			
            $("#example1 tbody").append(
              `
              <tr>
                <td>${previewRecord1.Year}</td>
                <td>${previewRecord1.quarter}</td>
				<td>CourtName</td>
                <td>${previewRecord1.fileName}</td>
                
              </tr>
              `
            )
          });
		  $('#example1').DataTable();
        },
        error: function(xhr, textStatus, err) {
		 
          console.log("xhr: ", xhr);
          console.log("Error while fetching the preview data: ", err);
        }
      })
    });
  </script>
  @include('common.footer')