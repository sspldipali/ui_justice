@include('common.header')
@include('common.sidebar')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
<link href="{!! asset('css/delivery_his_page.css') !!}" rel="stylesheet">
<body class="app sidebar-fixed aside-menu-fixed sidebar-lg-show">
  <main class="main margin_top_66 margin_top_70">
    <header class="app-header navbar header_bk_co">
      <div class="whitecolor page_name">
        <h5 class="whitecolor"><a href="#" class="whitecolor">{{ trans('messages.Delivery_History') }}</h5>
      </div>
    </header>
    <div class="container-fluid margin_top_35">
      <div class="animated fadeIn">
        <div class="row ">
          <div class="col-sm-12">
            <h5 class="whitecolor font_size_15 display_flex"><a href="#" class=" text_font22 whitecolor">{{ trans('messages.Home') }}</a> / {{ trans('messages.Reports1') }} </h5>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <form class="form-horizontal" action="#" method="post">
                <div class="form-group padding_20_0">
                  <div class="row">
                    <div class="col-sm-1">
                      <label class="control-label" for="text">{{ trans('messages.Year') }}</label>
                    </div>
                    <div class="col-sm-2">
                      <input type="text" class="form-control form-control1"  placeholder="{{ trans('messages.Enter_Year') }}">
                    </div>
                    <div class="col-sm-1">
                      <label class="control-label " for="text">{{ trans('messages.Quarter') }}</label>
                    </div>
                    <div class="col-sm-2">
                      <select class="form-control form-control1 margin_left_m_15 margin_left_0">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                      </select>
                    </div>
                    <div class="col-sm-2">
                      <button type="button" class="btn btn-default btn_default">{{ trans('messages.Searching_For') }}</button>
                    </div>
                  </div>
                </div>
                <script>
                  var dt = new Date().toLocaleTimeString();
                  document.getElementById("datetime").innerHTML = dt.toLocaleString();
                </script>
              </form>
              <div class="card-body">
                <div class="border_cord">
                  <p class="col-sm-12 datadiv whitecolor padding_left_right_20">{{ trans('messages.Delivery_Report') }}</p>
                  <div class="col-sm-12 padding_left_right_10">
                  <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered">
                      <thead>
                        <tr class="tr_bk_color">
                          <th >{{ trans('messages.Year') }}</th>
                          <th >{{ trans('messages.Quarter') }}</th>
                          <th >{{ trans('messages.Time') }}</th>
                          <th >{{ trans('messages.Notice') }}</th>
                          <th >{{ trans('messages.Number_Of_Cases') }}</th>
                          <th >{{ trans('messages.Amount_Of_Receipt') }}</th>
                          <th >{{ trans('messages.Delivery_Date') }}</th>
                          <th >{{ trans('messages.Carrier') }}</th>
                          <th >{{ trans('messages.Status') }}</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>2018</td>
                          <td>4</td>
                          <td>1</td>
                          <td>Notifications 4/2018</td>
                          <td>100</td>
                          <td>30</td>
                          <td>24/12/2018 10:27</td>
                          <td>somchai</td>
                          <td>Accomplished</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <!-- CoreUI and necessary plugins-->
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
  <script src="{!! asset('vendors/popper.js/js/popper.min.js') !!}"
	type="text/javascript"></script>
  <script src="{!! asset('vendors/bootstrap/js/bootstrap.min.js') !!}"
	type="text/javascript"></script>
  <script src="{!! asset('vendors/pace-progress/js/pace.min.js') !!}"
	type="text/javascript"></script>
  <script src="{!! asset('vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') !!}"
	type="text/javascript"></script>
  <script src="{!! asset('vendors/@coreui/coreui/js/coreui.min.js') !!}"
	type="text/javascript"></script>

  <script>
   	$(document).ready(function () {
    $('.navbar-light .dmenu').hover(function () {
            $(this).find('.sm-menu').first().stop(true, true).slideDown(150);
        }, function () {
            $(this).find('.sm-menu').first().stop(true, true).slideUp(105)
        });
    });
    $(document).ready(function() {
          $('#example').DataTable();
        } );
  </script>
@include('common.footer')
