@include('common.header')
@include('common.sidebar')
<main class="main margin_top_66 margin_top_70">
  <header class="app-header navbar  navbar header_bk_co">
    <div class="whitecolor page_name">
      <h5 class="whitecolor"><a href="#" class="text_font22 whitecolor">{{ trans('messages.Pie_Graph') }}</h5>
    </div>
  </header>
  <div class="container-fluid margin_top_35">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12">
          <h5 class="whitecolor display_flex font_size_15p"><a href="{{ url('/home') }}" class="whitecolor">{{ trans('messages.Home') }}</a> / {{ trans('messages.Reports2') }} / {{ trans('messages.Within_Court') }} </h5>
        </div>
      </div>
      <div class="row margin_top_15">
        <div class="col-sm-6">
          <div class="card">
            <div class=" card-header text_font22">{{ trans('messages.Pie_Graph_1') }}  
            </div>
            <div class="card-body">
              <label for="sel1">{{ trans('messages.Select_Pie_wi') }}</label>
               <select id="Juridiction_1" name="Juridiction" class="form-control"required onchange="drawChart_1()">
               </select>
			        <div class="loading1"><div id="loading-img"></div></div>   
              <div id="piechart">
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Pie_Graph_2') }}  
            </div>
            <div class="card-body">
              <label for="sel1">{{ trans('messages.Select_Pie_wi') }}</label>
              <select id="Juridiction_2" name="Juridiction" class="form-control"required onchange="drawChart_2()">
              </select>
			       <div class="loading2"><div id="loading-img"></div></div>   
              <div id="piechart1"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Pie_Graph_3') }}  3
            </div>
            <div class="card-body">
                <label for="sel1">{{ trans('messages.Select_Pie_wi') }}</label>
                <select id="Juridiction_3" name="Juridiction" class="form-control"required onchange="drawChart_3()">
                </select>
			      	<div class="loading3"><div id="loading-img"></div></div>   
              <div id="piechart2"></div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Pie_Graph_4') }}  
            </div>
            <div class="card-body">
              <label for="sel1">{{ trans('messages.Select_Pie_wi') }}</label>
              <select id="Juridiction_4" name="Juridiction" class="form-control"required onchange="drawChart_4()">
              </select>
			       <div class="loading4"><div id="loading-img"></div></div>   
              <div id="piechart3"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



</main>
@include('common.footer')


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
 $('#piechart').click( function() {
  $('.loading1').hide(); 
});

var url = '{{ Config::get('url.url') }}';
var token = '{{ Session::get('key') }}';

google.load("visualization", "1", { packages: ["corechart"] });
google.setOnLoadCallback(drawChart_1);
    
   function drawChart_1(Juristiction) {
      debugger;
      Juristiction = document.getElementById("Juridiction_1").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];

    $(".loading1").show();
    $.ajax({
   url: ""+url+"/pie_wi_api/"+Juristiction+"?token="+token+"",
	  dataType: "json",
	  type: "GET",
	
    success: function (data) {

         $('.loading1').hide();
         user_data = JSON.stringify(data);
         finaldata=[['Court', 'CaseCount']]
         for (i=0;i<data.length;i++){
           var innerarray=[]
           innerarray.push(data[i].Court,Number(data[i].CaseCount))
           finaldata.push(innerarray);
           innerarray=[];
           }
         console.log(finaldata);
         var data = google.visualization.arrayToDataTable(finaldata);
         console.log(data);

         var options = {
           title: 'สถิติจำนวนคดีการสืบทรัพย์ของแต่ละหน่วยศาลในไตรมาสปัจจุบัน'
         };
         var chart = new google.visualization.PieChart(document.getElementById('piechart'));
         chart.draw(data, options);
                                
        },
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                  //  alert("error");
                  $(".loading1").hide();
                }
        });
}
</script>
<script type="text/javascript">
$('#piechart1').click( function() {
  $('.loading2').hide(); 
});

    google.load("visualization", "1", { packages: ["corechart"] });
    google.setOnLoadCallback(drawChart_2);
    
   
    function drawChart_2(Juristiction) {
      debugger;
      Juristiction = document.getElementById("Juridiction_2").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];

      $(".loading2").show();
    $.ajax({
	  url: ""+url+"/pie_wi_api_2/"+Juristiction+"?token="+token+"",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	
	   success: function (data) {
         
          $('.loading2').hide();
          user_data = JSON.stringify(data);
          finaldata=[['Court', 'CaseCount']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Court,Number(data[i].CaseCount))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'สถิติจำนวนคดีการผ่อนชำระค่าปรับของแต่ละหน่วยศาลในไตรมาสปัจจุบัน'
          };
          var chart = new google.visualization.PieChart(document.getElementById('piechart1'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                 //   alert("error");
                 $(".loading2").hide();
                }
        });
}
</script> 

<script type="text/javascript">
 $('#piechart2').click( function() {
  $('.loading3').hide(); 
});

    google.load("visualization", "1", { packages: ["corechart"] });
    google.setOnLoadCallback(drawChart_3);
    function drawChart_3(Juristiction) {
     // debugger;
      Juristiction = document.getElementById("Juridiction_3").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];

    $(".loading3").show();
    $.ajax({
	  url: ""+url+"/pie_wi_api_3/"+Juristiction+"?token="+token+"",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	
	   success: function (data) {

          $('.loading3').hide();
          user_data = JSON.stringify(data);
          finaldata=[['Court', 'Debt']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Court,Number(data[i].Debt))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'สถิติจำนวนหนี้ทั้งหมดของแต่ละหน่วยศาลในไตรมาสปัจจุบัน'
          };
          var chart = new google.visualization.PieChart(document.getElementById('piechart2'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                    //alert("error");
                    $(".loading3").hide();
                }
        });
}
</script> 

<script type="text/javascript">
$('#piechart3').click( function() {
  $('.loading4').hide(); 
});
    google.load("visualization", "1", { packages: ["corechart"] });
    google.setOnLoadCallback(drawChart_4);
    
    
    function drawChart_4(Juristiction) {
     // debugger;
      Juristiction = document.getElementById("Juridiction_4").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];

    $(".loading4").show();
    $.ajax({
	  url: ""+url+"/pie_wi_api_4/"+Juristiction+"?token="+token+"",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	
	   success: function (data) {

          $('.loading4').hide();
          user_data = JSON.stringify(data);
          finaldata=[['Court', 'Debt']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Court,Number(data[i].Debt))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'สถิติจำนวนหนี้ที่บังคับคดีได้ของแต่ละหน่วยศาลในไตรมาสปัจจุบัน'
          };
          var chart = new google.visualization.PieChart(document.getElementById('piechart3'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                  //  alert("error");
                  $(".loading4").hide();
                }
        });
}

//GET juridiction ajax call
$(document).ready(function () {
  //debugger;
  $(".loading").show();
   $.ajax({
                type: "GET",
                url: ""+url+"/pie2",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                  $('.loading').hide();
                //  alert('sucess');
                var user_data=JSON.stringify(data);
                var result = JSON.parse(user_data);
                var orders1 = result.user;
                roles(result.user);
               
              // console.log(result.user[0]);
                },
                failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                    alert("error");
                    $(".loading").hide();
                  }
            });
    });
 function roles(user){ 
    
    $.each(user, function (index, value) {
        // APPEND OR INSERT DATA TO SELECT ELEMENT.
        $('#Juridiction_1').append('<option value="' + value.Id + '">' + value.Code + '-'+ value.Name +'</option>');
        $('#Juridiction_2').append('<option value="' + value.Id + '">' + value.Code + '-'+ value.Name +'</option>');
        $('#Juridiction_3').append('<option value="' + value.Id + '">' + value.Code + '-'+ value.Name +'</option>');
        $('#Juridiction_4').append('<option value="' + value.Id + '">' + value.Code + '-'+ value.Name +'</option>');
    });
  var Juridiction =  user[0].Id;
    drawChart_1(Juridiction);
    drawChart_2(Juridiction);
    drawChart_3(Juridiction);
    drawChart_4(Juridiction);
// alert(user);
}
</script> 



