@include('common.header')
@include('common.sidebar')
<main class="main margin_top_66 margin_top_70">
  <header class="app-header navbar header_bk_co">
    <div class="whitecolor page_name">
      <h5 class="whitecolor"><a href="#" class="text_font22 whitecolor">{{ trans('messages.Pie_Graph') }}</h5>
    </div>
  </header>
  <div class="container-fluid margin_top_35">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12">
          <h5 class="whitecolor display_flex font_size_15"><a href="{{ url('/home') }}" class="whitecolor">{{ trans('messages.Home') }}</a> / {{ trans('messages.Reports2') }} / {{ trans('messages.Including_All_Court') }} </h5>
        </div>
      </div>
      <div class="row margin_top_15 margin_top_0">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Pie_Graph_1') }}  
            </div>
            <div class="card-body">
			<div class="loading"><div id="loading-img"></div></div>
              <div id="piechart">
            </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Pie_Graph_2') }}  
            </div>
            <div class="card-body">
			<div class="loading"><div id="loading-img"></div></div>
              <div id="piechart1"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Pie_Graph_3') }}  
            </div>
            <div class="card-body">
			<div class="loading"><div id="loading-img"></div></div>
              <div id="piechart2"></div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Pie_Graph_4') }}  
            </div>
            <div class="card-body">
			<div class="loading"><div id="loading-img"></div></div>
              <div id="piechart3"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@include('common.footer')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
 $('#piechart').click( function() {
  $('.loading').hide(); 
});

var url = '{{ Config::get('url.url') }}';
var token = '{{ Session::get('key') }}';

  google.load("visualization", "1", { packages: ["corechart"] });
  google.setOnLoadCallback(drawChart);
  function drawChart() {
      var user_data;
      var finaldata=[];

    $(".loading").show();
    $.ajax({
	  url: ""+url+"/pie_in_api?token="+token+"",
	  dataType: "json",
	  type: "GET",
	   success: function (data) {
      $('.loading').hide();
          user_data = JSON.stringify(data);
          finaldata=[['Juridiction', 'CaseCourt']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Juristiction,Number(data[i].CaseCount))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'สถิติจำนวนคดีการสืบทรัพย์ของแต่ละภาคในไตรมาสปัจจุบัน'
          };
          var chart = new google.visualization.PieChart(document.getElementById('piechart'));
          
          chart.draw(data, options);
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                   // alert("error");
                   $(".loading").hide();
                }
        });
}
</script>

<script type="text/javascript">
 $('#piechart1').click( function() {
  $('.loading').hide(); 
});


google.load("visualization", "1", { packages: ["corechart"] });
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var user_data;
      var finaldata=[];

      $(".loading").show();
    $.ajax({
	  url: ""+url+"/pie_in_api_2?token="+token+"",
	  dataType: "json",
	  type: "GET",
	   success: function (data) {
      $('.loading').hide();
          user_data = JSON.stringify(data);
          finaldata=[['Juridiction', 'CaseCourt']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Juristiction,Number(data[i].CaseCount))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);
          var options = {
            title: 'สถิติจำนวนคดีการผ่อนชำระค่าปรับของแต่ละภาคในไตรมาสปัจจุบัน'
          };
          var chart = new google.visualization.PieChart(document.getElementById('piechart1'));
          chart.draw(data, options);
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                   //alert("error");
                   $(".loading").hide();
                }
        });
}
</script>

<script type="text/javascript">
 $('#piechart2').click( function() {
  $('.loading').hide(); 
});


    google.load("visualization", "1", { packages: ["corechart"] });
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var user_data;
      var finaldata=[];

      $(".loading3").show();
    $.ajax({
	  url: ""+url+"/pie_in_api_3?token="+token+"",
	  dataType: "json",
	  type: "GET",
	   success: function (data) {
      $('.loading').hide();
          user_data = JSON.stringify(data);
          finaldata=[['Juridiction', 'Debt']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Juristiction,Number(data[i].Debt))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);
          var options = {
            title: 'สถิติจำนวนหนี้ทั้งหมดของแต่ละภาคในไตรมาสปัจจุบัน'
          };
          var chart = new google.visualization.PieChart(document.getElementById('piechart2'));
          chart.draw(data, options);
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                   // alert("error");
                   $(".loading").hide();
                }
        });
}
</script>

<script type="text/javascript">
 $('#piechart3').click( function() {
  $('.loading').hide(); 
});


google.load("visualization", "1", { packages: ["corechart"] });
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var user_data;
      var finaldata=[];

      $(".loading").show();
    $.ajax({
	  url: ""+url+"/pie_in_api_4?token="+token+"",
	  dataType: "json",
	  type: "GET",
	   success: function (data) {
      $('.loading').hide();
          user_data = JSON.stringify(data);
          finaldata=[['Juridiction', 'Debt']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Juristiction,Number(data[i].Debt))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);
          var options = {
            title: 'สถิติจำนวนหนี้ที่บังคับคดีได้ของแต่ละภาคในไตรมาสปัจจุบัน'
          };
          var chart = new google.visualization.PieChart(document.getElementById('piechart3'));
          chart.draw(data, options);
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                    //alert("error");
                    $(".loading").hide();
                }
        });
}
</script>



