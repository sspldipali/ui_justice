@include('common.header')
@include('common.sidebar')
<main class="main margin_top_66 margin_top_70">
  <header class="app-header navbar header_bk_co">
    <div class="whitecolor page_name">
      <h5 class="whitecolor"><a href="#" class="text_font22 whitecolor">{{ trans('messages.Bar_Graph') }}</h5>
    </div>
  </header>
  <div class="container-fluid margin_top_35">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12">
          <h5 class="whitecolor display_flex font_size_15"><a href="{{ url('/home') }}"  class="whitecolor" >{{ trans('messages.Home') }}</a> / {{ trans('messages.Reports2') }} / {{ trans('messages.Within_Court') }} </h5>
        </div>
      </div>
      <div class="row margin_top_15 margin_top_0">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Bar_Graph_wi_1') }}  
            </div>
            <div class="card-body over_flow">
              <label for="sel1">{{ trans('messages.Select_bar_wi_1') }}</label>
               <select id="Juridiction_1" name="Juridiction_1" class="form-control"required onchange="drawChart_1()">
               </select>
               <label for="sel1">{{ trans('messages.Select_bar_wi_2') }}</label>
               <select id="precedingQtr_1" name="precedingQtr_1" class="form-control"required onchange="drawChart_1()">
               </select>
			   <div class="loading1"><div id="loading-img"></div></div>
              <div id="columnchart_values">
            </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Bar_Graph_wi_2') }}  
            </div>
            <div class="card-body over_flow">
            <label for="sel1">{{ trans('messages.Select_bar_wi_1') }}</label>
             <select id="Juridiction_2" name="Juridiction_2" class="form-control"required onchange="drawChart_2()">
               </select>
            <label for="sel1">{{ trans('messages.Select_bar_wi_2') }}</label>
               <select id="precedingQtr_2" name="precedingQtr_2" class="form-control"required onchange="drawChart_2()">
               </select>
			   <div class="loading2"><div id="loading-img"></div></div>
              <div id="columnchart_values1"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Bar_Graph_wi_3') }}  
            </div>
            <div class="card-body over_flow">
            <label for="sel1">{{ trans('messages.Select_bar_wi_1') }}</label>
            <select id="Juridiction_3" name="Juridiction_3" class="form-control"required onchange="drawChart_2()">
               </select>
            <label for="sel1">{{ trans('messages.Select_bar_wi_2') }}</label>
               <select id="precedingQtr_3" name="precedingQtr_3" class="form-control"required onchange="drawChart_2()">
               </select>
			   <div class="loading3"><div id="loading-img"></div></div>
              <div id="columnchart_values2"></div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Bar_Graph_wi_4') }}  
            </div>
            <div class="card-body over_flow">
            <label for="sel1">{{ trans('messages.Select_bar_wi_1') }}</label>
               <select id="Juridiction_4" name="Juridiction_4" class="form-control"required onchange="drawChart_4()">
               </select>
            <label for="sel1">{{ trans('messages.Select_bar_wi_2') }}</label>
               <select id="precedingQtr_4" name="precedingQtr_4" class="form-control"required onchange="drawChart_4()">
               </select>
			   <div class="loading4"><div id="loading-img"></div></div>
              <div id="columnchart_values3"></div>
            </div>
          </div>
        </div>
       <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Bar_Graph_wi_5') }}  
            </div>
            <div class="card-body over_flow">
            <label for="sel1">{{ trans('messages.Select_bar_wi_1') }}</label>
               <select id="Juridiction_5" name="Juridiction_5" class="form-control"required onchange="drawChart_5()">
               </select>
            <label for="sel1">{{ trans('messages.Select_bar_wi_2') }}</label>
               <select id="precedingQtr_5" name="precedingQtr_5" class="form-control"required onchange="drawChart_5()">
               </select>
			   <div class="loading5"><div id="loading-img"></div></div>
              <div id="columnchart_values4"></div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Bar_Graph_wi_6') }}  
            </div>
            <div class="card-body over_flow">
            <label for="sel1">{{ trans('messages.Select_bar_wi_1') }}</label>
               <select id="Juridiction_6" name="Juridiction_6" class="form-control"required onchange="drawChart_6()">
               </select>
            <label for="sel1">{{ trans('messages.Select_bar_wi_2') }}</label>
               <select id="precedingQtr_6" name="precedingQtr_6" class="form-control"required onchange="drawChart_6()">
               </select>
			   <div class="loading6"><div id="loading-img"></div></div>
              <div id="columnchart_values5"></div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
           <div class="card-header text_font22">{{ trans('messages.Bar_Graph_wi_7') }}  
            </div>
            <div class="card-body over_flow">
            <label for="sel1">{{ trans('messages.Select_bar_wi_1') }}</label>
           <select id="Juridiction_7" name="Juridiction_7" class="form-control"required onchange="drawChart_7()">
               </select>
            <label for="sel1">{{ trans('messages.Select_bar_wi_2') }}</label>
               <select id="precedingQtr_7" name="precedingQtr_7" class="form-control"required onchange="drawChart_7()">
               </select>
			   <div class="loading7"><div id="loading-img"></div></div>
              <div id="columnchart_values6"></div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Bar_Graph_wi_8') }}  
            </div>
            <div class="card-body over_flow">
           <label for="sel1">{{ trans('messages.Select_bar_wi_1') }}</label>
               <select id="Juridiction_8" name="Juridiction_8" class="form-control"required onchange="drawChart_8()">
               </select>
            <label for="sel1">{{ trans('messages.Select_bar_wi_2') }}</label>
               <select id="precedingQtr_8" name="precedingQtr_8" class="form-control"required onchange="drawChart_8()">
               </select>
			   <div class="loading8"><div id="loading-img"></div></div>
              <div id="columnchart_values7"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@include('common.footer')

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
$('#columnchart_values').click( function() {
  $('.loading1').hide(); 
});

var url = '{{ Config::get('url.url') }}';
var token = '{{ Session::get('key') }}';

google.load("visualization", "1", { packages: ["corechart"] });
    google.setOnLoadCallback(drawChart_1);
    
      function drawChart_1(Juristiction, PrecedingQtr) {
        debugger;
      Juristiction = document.getElementById("Juridiction_1").value; 
      PrecedingQtr = document.getElementById("precedingQtr_1").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];
      $(".loading1").show();
    $.ajax({
      url: ""+url+"/bar_wi_api_9/"+Juristiction+"/"+PrecedingQtr+"?token="+token+"",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	   success: function (data) {
      $('.loading1').hide();
 
          user_data = JSON.stringify(data);
          finaldata=[['Court', 'Qtr1','Qtr2','Qtr3','Qtr4','Qtr5','Qtr6','Qtr7','Qtr8','Qtr9','Qtr10','Qtr11','Qtr12']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Court,Number(data[i].Qtr1),Number(data[i].Qtr2),Number(data[i].Qtr3),Number(data[i].Qtr4),Number(data[i].Qtr5),Number(data[i].Qtr6),Number(data[i].Qtr7),Number(data[i].Qtr8),Number(data[i].Qtr9),Number(data[i].Qtr10),Number(data[i].Qtr11),Number(data[i].Qtr12))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'เปรียบเทียบจำนวนคดีการสืบทรัพย์ของแต่ละหน่วยศาลตามไตรมาสย้อนหลัง'
          };
          var chart = new google.visualization.ColumnChart(document.getElementById('columnchart_values'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                  $('.loading1').hide();
                    //alert("error");
                }
        });
}

</script> 

<script type="text/javascript">
$('#columnchart_values1').click( function() {
  $('.loading2').hide(); 
});


google.load("visualization", "1", { packages: ["corechart"] });
google.setOnLoadCallback(drawChart_2);
    
    function drawChart_2(Juristiction, PrecedingQtr) {
      Juristiction = document.getElementById("Juridiction_2").value; 
      PrecedingQtr = document.getElementById("precedingQtr_2").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];
      $(".loading2").show();
    $.ajax({
      url: ""+url+"/bar_wi_api_10/"+Juristiction+"/"+PrecedingQtr+"?token="+token+"",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	
	   success: function (data) {
      $('.loading2').hide();
 
          user_data = JSON.stringify(data);
          finaldata=[['Court', 'Qtr1','Qtr2','Qtr3','Qtr4','Qtr5','Qtr6','Qtr7','Qtr8','Qtr9','Qtr10','Qtr11','Qtr12']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Court,Number(data[i].Qtr1),Number(data[i].Qtr2),Number(data[i].Qtr3),Number(data[i].Qtr4),Number(data[i].Qtr5),Number(data[i].Qtr6),Number(data[i].Qtr7),Number(data[i].Qtr8),Number(data[i].Qtr9),Number(data[i].Qtr10),Number(data[i].Qtr11),Number(data[i].Qtr12))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'เปรียบเทียบจำนวนคดีการผ่อนชำระค่าปรับของแต่ละหน่วยศาลตามไตรมาสย้อนหลัง'
          };
          var chart = new google.visualization.ColumnChart(document.getElementById('columnchart_values1'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                  $('.loading2').hide();
                   // alert("error");
                }
        });
}

</script> 

<script type="text/javascript">
$('#columnchart_values2').click( function() {
  $('.loading3').hide(); 
});


google.load("visualization", "1", { packages: ["corechart"] });
google.setOnLoadCallback(drawChart_3);
    
    function drawChart_3(Juristiction, PrecedingQtr) {
      Juristiction = document.getElementById("Juridiction_3").value; 
      PrecedingQtr = document.getElementById("precedingQtr_3").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];
      $(".loading3").show();
    $.ajax({
      url: ""+url+"/bar_wi_api_11/"+Juristiction+"/"+PrecedingQtr+"?token="+token+"",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	
	   success: function (data) {
      $('.loading3').hide();
 
          user_data = JSON.stringify(data);
          finaldata=[['Court', 'Qtr1','Qtr2','Qtr3','Qtr4','Qtr5','Qtr6','Qtr7','Qtr8','Qtr9','Qtr10','Qtr11','Qtr12']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Court,Number(data[i].Qtr1),Number(data[i].Qtr2),Number(data[i].Qtr3),Number(data[i].Qtr4),Number(data[i].Qtr5),Number(data[i].Qtr6),Number(data[i].Qtr7),Number(data[i].Qtr8),Number(data[i].Qtr9),Number(data[i].Qtr10),Number(data[i].Qtr11),Number(data[i].Qtr12))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'เปรียบเทียบจำนวนหนี้ทั้งหมดของแต่ละหน่วยศาลตามไตรมาสย้อนหลัง'
          };
          var chart = new google.visualization.ColumnChart(document.getElementById('columnchart_values2'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                  $('.loading3').hide();
                  //  alert("error");
                }
        });
}

</script> 

<script type="text/javascript">
$('#columnchart_values3').click( function() {
  $('.loading4').hide(); 
});


google.load("visualization", "1", { packages: ["corechart"] });
google.setOnLoadCallback(drawChart_4);
    
    function drawChart_4(Juristiction, PrecedingQtr) {
      Juristiction = document.getElementById("Juridiction_4").value; 
      PrecedingQtr = document.getElementById("precedingQtr_4").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];
      $(".loading4").show();
    $.ajax({
      url: ""+url+"/bar_wi_api_12/"+Juristiction+"/"+PrecedingQtr+"?token="+token+"",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	
	   success: function (data) {
      $('.loading4').hide();
 
          user_data = JSON.stringify(data);
          finaldata=[['Court', 'Qtr1','Qtr2','Qtr3','Qtr4','Qtr5','Qtr6','Qtr7','Qtr8','Qtr9','Qtr10','Qtr11','Qtr12']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Court,Number(data[i].Qtr1),Number(data[i].Qtr2),Number(data[i].Qtr3),Number(data[i].Qtr4),Number(data[i].Qtr5),Number(data[i].Qtr6),Number(data[i].Qtr7),Number(data[i].Qtr8),Number(data[i].Qtr9),Number(data[i].Qtr10),Number(data[i].Qtr11),Number(data[i].Qtr12))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'เปรียบเทียบจำนวนหนี้ที่บังคับคดีได้ของแต่ละหน่วยศาลตามไตรมาสย้อนหลัง'
          };
          var chart = new google.visualization.ColumnChart(document.getElementById('columnchart_values3'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                  $('.loading4').hide();
                   // alert("error");
                }
        });
}

</script> 

<script type="text/javascript">
$('#columnchart_values4').click( function() {
  $('.loading5').hide(); 
});


google.load("visualization", "1", { packages: ["corechart"] });
google.setOnLoadCallback(drawChart_5);
    
    function drawChart_5(Juristiction, PrecedingQtr) {
      Juristiction = document.getElementById("Juridiction_5").value; 
      PrecedingQtr = document.getElementById("precedingQtr_5").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];
      $(".loading5").show();
    $.ajax({
	  url: ""+url+"/bar_wi_api_13/"+Juristiction+"/"+PrecedingQtr+"?token="+token+"",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	   success: function (data) {
      $('.loading5').hide();
 
          user_data = JSON.stringify(data);
          finaldata=[['Court', 'Qtr1','Qtr2','Qtr3','Qtr4','Qtr5','Qtr6','Qtr7','Qtr8','Qtr9','Qtr10','Qtr11','Qtr12']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Court,Number(data[i].Qtr1),Number(data[i].Qtr2),Number(data[i].Qtr3),Number(data[i].Qtr4),Number(data[i].Qtr5),Number(data[i].Qtr6),Number(data[i].Qtr7),Number(data[i].Qtr8),Number(data[i].Qtr9),Number(data[i].Qtr10),Number(data[i].Qtr11),Number(data[i].Qtr12))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'เปรียบเทียบจำนวนคดีการสืบทรัพย์ของแต่ละหน่วยศาลตามปีงบประมาณย้อนหลัง'
          };
          var chart = new google.visualization.ColumnChart(document.getElementById('columnchart_values4'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                  $('.loading5').hide();
                   // alert("error");
                }
        });
}

</script> 

<script type="text/javascript">
$('#columnchart_values5').click( function() {
  $('.loading6').hide(); 
});


google.load("visualization", "1", { packages: ["corechart"] });
google.setOnLoadCallback(drawChart_6);
    
    function drawChart_6(Juristiction, PrecedingQtr) {
      Juristiction = document.getElementById("Juridiction_6").value; 
      PrecedingQtr = document.getElementById("precedingQtr_6").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];
      $(".loading6").show();
    $.ajax({
      url: ""+url+"/bar_wi_api_14/"+Juristiction+"/"+PrecedingQtr+"?token="+token+"",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	
	   success: function (data) {
      $('.loading6').hide();
 
          user_data = JSON.stringify(data);
          finaldata=[['Court', 'Qtr1','Qtr2','Qtr3','Qtr4','Qtr5','Qtr6','Qtr7','Qtr8','Qtr9','Qtr10','Qtr11','Qtr12']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Court,Number(data[i].Qtr1),Number(data[i].Qtr2),Number(data[i].Qtr3),Number(data[i].Qtr4),Number(data[i].Qtr5),Number(data[i].Qtr6),Number(data[i].Qtr7),Number(data[i].Qtr8),Number(data[i].Qtr9),Number(data[i].Qtr10),Number(data[i].Qtr11),Number(data[i].Qtr12))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'เปรียบเทียบจำนวนคดีการผ่อนชำระค่าปรับของแต่ละหน่วยศาลตามปีงบประมาณย้อนหลัง'
          };
          var chart = new google.visualization.ColumnChart(document.getElementById('columnchart_values5'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                  $('.loading6').hide();
                   // alert("error");
                }
        });
}

</script> 

<script type="text/javascript">
$('#columnchart_values6').click( function() {
  $('.loading7').hide(); 
});


google.load("visualization", "1", { packages: ["corechart"] });
google.setOnLoadCallback(drawChart_7);
    
    function drawChart_7(Juristiction, PrecedingQtr) {
      Juristiction = document.getElementById("Juridiction_7").value; 
      PrecedingQtr = document.getElementById("precedingQtr_7").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];
      $(".loading7").show();
    $.ajax({
      url: ""+url+"/bar_wi_api_15/"+Juristiction+"/"+PrecedingQtr+"?token="+token+"",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	
	   success: function (data) {
      $('.loading7').hide();
 
          user_data = JSON.stringify(data);
          finaldata=[['Court', 'Qtr1','Qtr2','Qtr3','Qtr4','Qtr5','Qtr6','Qtr7','Qtr8','Qtr9','Qtr10','Qtr11','Qtr12']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Court,Number(data[i].Qtr1),Number(data[i].Qtr2),Number(data[i].Qtr3),Number(data[i].Qtr4),Number(data[i].Qtr5),Number(data[i].Qtr6),Number(data[i].Qtr7),Number(data[i].Qtr8),Number(data[i].Qtr9),Number(data[i].Qtr10),Number(data[i].Qtr11),Number(data[i].Qtr12))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'เปรียบเทียบจำนวนหนี้ทั้งหมดของแต่ละหน่วยศาลตามปีงบประมาณย้อนหลัง'
          };
          var chart = new google.visualization.ColumnChart(document.getElementById('columnchart_values6'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                  $('.loading7').hide();
                   // alert("error");
                }
        });
}

</script> 

<script type="text/javascript">
$('#columnchart_values7').click( function() {
  $('.loading8').hide(); 
});


google.load("visualization", "1", { packages: ["corechart"] });
google.setOnLoadCallback(drawChart_8);
    
    function drawChart_8(Juristiction, PrecedingQtr) {
      Juristiction = document.getElementById("Juridiction_8").value; 
      PrecedingQtr = document.getElementById("precedingQtr_8").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];
      $(".loading8").show();
    $.ajax({
      url: ""+url+"/bar_wi_api_16/"+Juristiction+"/"+PrecedingQtr+"?token="+token+"",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	
	   success: function (data) {
      $('.loading8').hide();
 
          user_data = JSON.stringify(data);
          finaldata=[['Court', 'Qtr1','Qtr2','Qtr3','Qtr4','Qtr5','Qtr6','Qtr7','Qtr8','Qtr9','Qtr10','Qtr11','Qtr12']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Court,Number(data[i].Qtr1),Number(data[i].Qtr2),Number(data[i].Qtr3),Number(data[i].Qtr4),Number(data[i].Qtr5),Number(data[i].Qtr6),Number(data[i].Qtr7),Number(data[i].Qtr8),Number(data[i].Qtr9),Number(data[i].Qtr10),Number(data[i].Qtr11),Number(data[i].Qtr12))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'เปรียบเทียบจำนวนหนี้ที่บังคับคดีได้ของแต่ละหน่วยศาลตามปีงบประมาณย้อนหลัง'
          };
          var chart = new google.visualization.ColumnChart(document.getElementById('columnchart_values7'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                  $('.loading8').hide();
                   // alert("error");
                }
        });
}

//GET preceding Juridiction ajax call
$(document).ready(function () {
  $(".loading").show();
  debugger;
   $.ajax({
                type: "GET",
                url: ""+url+"/pie2",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                  $('.loading').hide();
                
                var user_data=JSON.stringify(data);
                var result = JSON.parse(user_data);
                var orders1 = result.user;
                Juridiction(result.user);
               
              // console.log(result.user[0]);
                },
                failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                    //alert("error");
                    $('.loading').hide();
                  }
            });
    });
 //GET preceding qaurter ajax call
 $(document).ready(function () {
  $(".loading").show();
  debugger;
   $.ajax({
                type: "GET",
                url: ""+url+"/pie3",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                  $('.loading').hide();
                
                var user_data=JSON.stringify(data);
                var result = JSON.parse(user_data);
                var orders1 = result.user;
                qtr(result.user);
               
              // console.log(result.user[0]);
                },
                failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                  $('.loading').hide();
                   // alert("error");
                  }
            });
    });
 function Juridiction(user){ 
    
    $.each(user, function (index, value) {
        // APPEND OR INSERT DATA TO SELECT ELEMENT.
        $('#Juridiction_1').append('<option value="' + value.Id + '">' + value.Code + '-'+ value.Name +'</option>');
        $('#Juridiction_2').append('<option value="' + value.Id + '">' + value.Code + '-'+ value.Name +'</option>');
        $('#Juridiction_3').append('<option value="' + value.Id + '">' + value.Code + '-'+ value.Name +'</option>');
        $('#Juridiction_4').append('<option value="' + value.Id + '">' + value.Code + '-'+ value.Name +'</option>');
        $('#Juridiction_5').append('<option value="' + value.Id + '">' + value.Code + '-'+ value.Name +'</option>');
        $('#Juridiction_6').append('<option value="' + value.Id + '">' + value.Code + '-'+ value.Name +'</option>');
        $('#Juridiction_7').append('<option value="' + value.Id + '">' + value.Code + '-'+ value.Name +'</option>');
        $('#Juridiction_8').append('<option value="' + value.Id + '">' + value.Code + '-'+ value.Name +'</option>');

    });
}
function qtr(user){ 
    
    $.each(user, function (index, value) {
        // APPEND OR INSERT DATA TO SELECT ELEMENT.
        $('#precedingQtr_1').append('<option value="' + value.PrecedingQtr + '">' + value.PrecedingQtr + '</option>');
        $('#precedingQtr_2').append('<option value="' + value.PrecedingQtr + '">' + value.PrecedingQtr + '</option>');
        $('#precedingQtr_3').append('<option value="' + value.PrecedingQtr + '">' + value.PrecedingQtr + '</option>');
        $('#precedingQtr_4').append('<option value="' + value.PrecedingQtr + '">' + value.PrecedingQtr + '</option>');
        $('#precedingQtr_5').append('<option value="' + value.PrecedingQtr + '">' + value.PrecedingQtr + '</option>');
        $('#precedingQtr_6').append('<option value="' + value.PrecedingQtr + '">' + value.PrecedingQtr + '</option>');
        $('#precedingQtr_7').append('<option value="' + value.PrecedingQtr + '">' + value.PrecedingQtr + '</option>');
        $('#precedingQtr_8').append('<option value="' + value.PrecedingQtr + '">' + value.PrecedingQtr + '</option>');

    });
}

</script>


