@include('common.header')
@include('common.sidebar')
<main class="main margin_top_66 margin_top_70">
  <header class="app-header navbar header_bk_co">
    <div class="page_name">
      <h5 class="whitecolor"><a href="#" class="text_font22 whitecolor">{{ trans('messages.Line_Graph') }}</h5>
    </div>
  </header>
  <div class="container-fluid margin_top_35">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12">
          <h5 class="whitecolor display_flex font_size_15px"><a href="{{ url('/home') }}" class="whitecolor">{{ trans('messages.Home') }}</a> / {{ trans('messages.Reports2') }} / {{ trans('messages.Including_All_Court') }} </h5>
        </div>
      </div>
      <div class="row margin_top_15 margin_top_0">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Line_Graph_1') }}  
            </div>
            <div class="card-body over_flow">
              <label for="sel1">{{ trans('messages.Select_line_in_chart') }}</label>
               <select id="FiscalYear_1" name="FiscalYear" class="form-control"required onchange="drawChart_1()">
               </select>
			    <div class="loading1"><div id="loading-img"></div></div>
              <div id="curve_chart">
            </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Line_Graph_2') }}  
            </div>
            <div class="card-body over_flow">
              <label for="sel1">{{ trans('messages.Select_line_in_chart') }}</label>
               <select id="FiscalYear_2" name="FiscalYear" class="form-control"required onchange="drawChart_2()">
               </select>
			    <div class="loading2"><div id="loading-img"></div></div>
              <div id="curve_chart1"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Line_Graph_3') }}  
            </div>
            <div class="card-body over_flow">
            <label for="sel1">{{ trans('messages.Select_line_in_chart') }}</label>
               <select id="FiscalYear_3" name="FiscalYear" class="form-control"required onchange="drawChart_3()">
               </select>
			    <div class="loading3"><div id="loading-img"></div></div>
              <div id="curve_chart2"></div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header text_font22">{{ trans('messages.Line_Graph_4') }}  
            </div>
            <div class="card-body over_flow">
            <label for="sel1">{{ trans('messages.Select_line_in_chart') }}</label>
               <select id="FiscalYear_4" name="FiscalYear" class="form-control"required onchange="drawChart_4()">
               </select>
			    <div class="loading4"><div id="loading-img"></div></div>
              <div id="curve_chart3"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@include('common.footer')

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
$('#curve_chart').click( function() {
  $('.loading1').hide(); 
});


var url = '{{ Config::get('url.url') }}';
var token = '{{ Session::get('key') }}';

google.load("visualization", "1", { packages: ["corechart"] });
google.setOnLoadCallback(drawChart_1);
    
    function drawChart_1(FiscalYear) {
      debugger;
      FiscalYear = document.getElementById("FiscalYear_1").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];

    $(".loading1").show();
    $.ajax({
    
	  url: ""+url+"/line_in_api_5/"+FiscalYear+"?token="+token+"",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	   success: function (data) {

    $('.loading1').hide();
          user_data = JSON.stringify(data);
          finaldata=[['Juridiction', 'Qtr1','Qtr2','Qtr3','Qtr4']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Juristiction,Number(data[i].Qtr1),Number(data[i].Qtr2),Number(data[i].Qtr3),Number(data[i].Qtr4))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'สถิติจำนวนคดีการสืบทรัพย์ของแต่ละภาคในปีงบประมาณทีระบุ'
          };
          var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                //alert("error");
                $('.loading1').hide();
                }
        });
}

</script> 

<script type="text/javascript">
$('#curve_chart1').click( function() {
  $('.loading2').hide(); 
});



google.load("visualization", "1", { packages: ["corechart"] });
google.setOnLoadCallback(drawChart_2);
    
    function drawChart_2(FiscalYear) {
      debugger;
      FiscalYear = document.getElementById("FiscalYear_2").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];
      $(".loading2").show();

    $.ajax({
	  url: ""+url+"/line_in_api_6/"+FiscalYear+"?token="+token+"",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	
	   success: function (data) {
      $('.loading2').hide();
 
          user_data = JSON.stringify(data);
          finaldata=[['Juristiction', 'Qtr1','Qtr2','Qtr3','Qtr4']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Juristiction,Number(data[i].Qtr1),Number(data[i].Qtr2),Number(data[i].Qtr3),Number(data[i].Qtr4))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'สถิติจำนวนคดีการผ่อนชำระค่าปรับของแต่ละภาคในปีงบประมาณทีระบุ'
          };
          var chart = new google.visualization.LineChart(document.getElementById('curve_chart1'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                  //  alert("error");
                  $('.loading2').hide();
                }
        });
}

</script> 

<script type="text/javascript">
$('#curve_chart2').click( function() {
  $('.loading3').hide(); 
});



google.load("visualization", "1", { packages: ["corechart"] });
google.setOnLoadCallback(drawChart_3);
    
    function drawChart_3(FiscalYear) {
     // debugger;
      FiscalYear = document.getElementById("FiscalYear_3").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];
      $(".loading3").show();

    $.ajax({
	  url: ""+url+"/line_in_api_7/"+FiscalYear+"?token="+token+"",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	   success: function (data) {
      $('.loading3').hide();
 
          user_data = JSON.stringify(data);
          finaldata=[['Juristiction', 'Qtr1','Qtr2','Qtr3','Qtr4']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Juristiction,Number(data[i].Qtr1),Number(data[i].Qtr2),Number(data[i].Qtr3),Number(data[i].Qtr4))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'สถิติจำนวนหนี้ทั้งหมดของแต่ละภาคในปีงบประมาณทีระบุ'
          };
          var chart = new google.visualization.LineChart(document.getElementById('curve_chart2'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                   // alert("error");
                   $('.loading3').hide();
                }
        });
}

</script> 

<script type="text/javascript">
$('#curve_chart3').click( function() {
  $('.loading4').hide(); 
});



google.load("visualization", "1", { packages: ["corechart"] });
google.setOnLoadCallback(drawChart_4);
    
    function drawChart_4(FiscalYear) {
      debugger;
      FiscalYear = document.getElementById("FiscalYear_4").value; 
      var user_data;
      innerarray=[];
      var finaldata=[];
      $(".loading4").show();

      
    $.ajax({
	  url: ""+url+"/line_in_api_8/"+FiscalYear+"?token="+token+"",
    
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
	
	   success: function (data) {
      $('.loading4').hide();
            
          user_data = JSON.stringify(data);
          finaldata=[['Juristiction', 'Qtr1','Qtr2','Qtr3','Qtr4']]
          for (i=0;i<data.length;i++){
            var innerarray=[]
            innerarray.push(data[i].Juristiction,Number(data[i].Qtr1),Number(data[i].Qtr2),Number(data[i].Qtr3),Number(data[i].Qtr4))
            finaldata.push(innerarray);
            innerarray=[];
            }
          console.log(finaldata);
          var data = google.visualization.arrayToDataTable(finaldata);
          console.log(data);

          var options = {
            title: 'สถิติจำนวนหนี้ที่บังคับคดีได้ของแต่ละภาคในปีงบประมาณทีระบุ'
          };
          var chart = new google.visualization.LineChart(document.getElementById('curve_chart3'));
          chart.draw(data, options);
                                 
	},
    failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                    //alert("error");
                    $('.loading4').hide();
                }
        });
}

//GET FiscalYear ajax call
$(document).ready(function () {
  $(".loading").show();
  //debugger;
   $.ajax({
                type: "GET",
                url: ""+url+"/pie1",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                  $('.loading').hide();
                
                var user_data=JSON.stringify(data);
                var result = JSON.parse(user_data);
                var orders1 = result.user;
                FiscalYear(result.user);
               
              // console.log(result.user[0]);
                },
                failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                   // alert("error");
                   $('.loading').hide();
                  }
            });
    });
 function FiscalYear(user){ 
    
    $.each(user, function (index, value) {
        // APPEND OR INSERT DATA TO SELECT ELEMENT.
         // APPEND OR INSERT DATA TO SELECT ELEMENT.
        $('#FiscalYear_1').append('<option value="' + value.FiscalYear + '">' + value.FiscalYear + '</option>');
        $('#FiscalYear_2').append('<option value="' + value.FiscalYear + '">' + value.FiscalYear + '</option>');
        $('#FiscalYear_3').append('<option value="' + value.FiscalYear + '">' + value.FiscalYear + '</option>');
        $('#FiscalYear_4').append('<option value="' + value.FiscalYear + '">' + value.FiscalYear + '</option>');
    });
    var FiscalYear =  user[0].FiscalYear;
    drawChart_1(FiscalYear);
    drawChart_2(FiscalYear);
    drawChart_3(FiscalYear);
    drawChart_4(FiscalYear);
}
</script>


