<!DOCTYPE html>
<html lang="en">
  <head>
  <?php App::setLocale('th');?>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>{{ trans('messages.Justice') }}</title>
    <!-- Main styles for this application-->
	  <link href="{!! asset('vendors/css/style.css') !!}" rel="stylesheet">
    <link href="{!! asset('vendors/css/allcss.css') !!}"rel="stylesheet">
    <link href="{!! asset('vendors/pace-progress/css/pace.min.css')!!}" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script src="{!! asset('vendors/jquery/js/jquery.min.js') !!}" type="text/javascript"></script>
    <script type="text/javascript" src="{!! asset('vendors/js/jquery-2.1.4.js') !!}"></script>
    <script src="{!! asset('vendors/bootstrap/js/bootstrap.min.js') !!}"type="text/javascript"></script>
    <script type="text/javascript" src="{!! asset('vendors/js/globalize.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendors/js/dx.webappjs.js') !!}"></script>
    <script src="{!! asset('vendors/js/common_functions.js') !!}"></script>
    <script src="{!! asset('vendors/js/bootbox.min.js') !!}"></script>
    <script>
      $(document).ready(function() {
      	$(".loading").hide();
   	})
     function reset_password() {
          var newPassword = document.getElementById('newPassword').value;
          var url = '{{ Config::get('url.url') }}';
          var baseURL = {!! json_encode(url('/')) !!};
          var qrStr = window.location.href;
            qrStr = qrStr.split("/");
            var id = qrStr [qrStr.length -1];
            if ($("#newPassword").val() == "") {
        		bootbox.alert(" {{ trans('messages.Please_Enter_New_Password') }} !", function() {});
            } else {
				$(".loading").show();
              $.ajax({
              url: ""+url+"/resetForgotPassword/"+id+"",
                dataType: "json",
                type: "POST",
                data: {newPassword},
                success: function (data) {
					  $('.loading').hide();
                    bootbox.alert("{{ trans('messages.Password_Reset_Successfully') }}.", function() {
                    	 window.location = ""+baseURL;
                    });
                  },
                  error: function (result) {
					 $(".loading").hide();
                  }

              });
            }
        }
    </script>
    <style>
      .btn-primary {
        color: #fff;
        background-color: #43768a;
        border-color: #43768a;
      }
      .text_colo_white{
        color:#fff;
      }
      .card {
        border: 1px solid #274450;
        border-radius: 0.25rem;
      }
      .login_btn{
        margin-left:10px;
      }
      .text_align_c{
        text-align: center;
      }
      .card-body {
        padding: 1rem !important;
      }
      .text_align_end{
        text-align: end;
      }
      body{
        overflow: hidden;
        background: linear-gradient(to bottom, #274450b3 0%, #1c343ebf 100%, transparent), url({{url('/img/backgrd.png') }}) no-repeat;
        background-size: cover;
      }
      .col-form-label {
        padding-left: 7px;
      }
      .error {
          color: red;
      }
      #errorMsg{
        color:red;
      }
      label.required:after {content: " *"; color: red;}
      #loading-img {
          background: url(https://i1.wp.com/cdnjs.cloudflare.com/ajax/libs/galleriffic/2.0.1/css/loader.gif) center center no-repeat;
          height: 100%;
      }
      .loading {
          background: #a3a3a3;
          display: none;
          position: absolute;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          opacity: 0.5;
          z-index: 20;
      }
      .reset_btn{
        border-radius: 6px; width:100%;
      }
      .card_bg_color{
        background-color: #274450;
      }
      .h1_text{
        color: #ffffff;margin-left: 40%;font-size: 24px;
      }
      .margin_top_4{
        margin-top: 4%;
      }
      @media screen and (min-width: 320px) and (max-width: 767px){
        .reset_btn{
          border-radius: 6px; width:100%;
        }
        .h1_text{
          margin-left: 0%;
        }
      }
      @media screen and (min-width: 768px) and (max-width: 1025px){
        .card_div{
          width: 156%;
          margin-left: -25%;
        }
      }
    </style>
  </head>

  <body style="" class="app flex-row align-items-center">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-sm-5">
          <div class="card p-4 card_bg_color card_div">
            <div class="card-body">
              <h1 class="h1_text text_font">{{ trans('messages.Reset_Password') }}</h1>
              <form method ="post" action='/resetForgotPassword/' name="myform" class="margin_top_4">
                @csrf
						    <span id="errorMsg"></span>
						    <div class="loading"><div id="loading-img"></div></div>
                <div class="form-group row">
                  <label for="newPassword" class="text_font required col-sm-4 whitecolor" required>{{ trans('messages.New_Password') }}</label>
                  <div class="col-sm-8">
                    <input id="newPassword" type="password" class="text_font22 form-control @error('newPassword') is-invalid @enderror" name="newPassword" value="{{ old('newPassword') }}" required autocomplete="email" autofocus data-required-message="Password is required.">
                    @error('newPassword')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>

              <div class="form-group row">
                <div class="offset-sm-4 col-sm-8">
                  <div class="row">
                    <div class="col-sm-6">
                      <button  class="text_font22 btn btn-primary px-4 reset_btn" onclick="reset_password()" type="button">
                      {{ trans('messages.Submit') }}
                      </button>
                    </div>
                    <div class="col-sm-6">
                      <button class="text_font22 btn btn-primary px-4 reset_btn margin_top_10" type="reset" >
                      {{ trans('messages.Cancel') }}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
</body>
</html>

