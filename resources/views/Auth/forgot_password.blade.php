﻿<!DOCTYPE html>
<html lang="en">
  <head>
  <?php App::setLocale('th'); ?>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>{{ trans('messages.Justice') }}</title>
    <link href="{!! asset('css/style.css') !!}" rel="stylesheet">
    <link href="{!! asset('vendors/css/allcss.css') !!}"rel="stylesheet">
    <link href="{!! asset('vendors/pace-progress/css/pace.min.css') !!}" rel="stylesheet">
    <script src="{!! asset('vendors/jquery/js/jquery.min.js') !!}" type="text/javascript"></script>
    <script type="text/javascript" src="{!! asset('vendors/js/jquery-2.1.4.js') !!}"></script>
    <script src="{!! asset('vendors/bootstrap/js/bootstrap.min.js') !!}"type="text/javascript"></script>
    <script type="text/javascript" src="{!! asset('vendors/js/globalize.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendors/js/dx.webappjs.js') !!}"></script>
    <script src="{!! asset('js/common_functions.js') !!}"></script>
    <script src="{!! asset('js/bootbox.min.js') !!}"></script>
    <script>
      $(document).ready(function() {
        $(".loading").hide();
   	  })
      function sendResetLink() {
        var url = '{{ Config::get('url.url') }}';
        var baseURL = {!! json_encode(url('/')) !!};
        var email = document.getElementById('email').value;
        if ($("#email").val() == "") {
		    bootbox.alert("{{ trans('messages.Enter_Register_Id') }}", function() {});
        } else {
          $(".loading").show();
          $.ajax({
      	    url: ""+url+"/sendResetLink",
	          dataType: "json",
	          type: "POST",
	          data: {email},
	          success: function (data) {
              $('.loading').hide();
		          bootbox.alert("{{ trans('messages.Reset_link_send') }}", function(){
	            window.location = ""+baseURL;
              });
	          },
            failure: function (response) {
              alert("failed");
            },
            error: function (err, status, xhr) {
			   $(".loading").hide();
               console.log("Failure");
               console.log("response: ", err.responseJSON);
               $("#errorMsg").html(err.responseJSON.message);
        }
	        });
        }
      }
    </script>
    <style>
      .btn-primary {
        color: #fff;
        background-color: #43768a;
        border-color: #43768a;
      }
      .text_colo_white{
        color:#fff;
      }
      .card {
        border: 1px solid #274450;
        border-radius: 0.25rem;
      }
      .login_btn{
        margin-left:10px;
      }
      .text_align_c{
        text-align: center;
      }
      .card-body {
        padding: 1rem !important;
      }
      .text_align_end{
        text-align: end;
      }
      body{
        overflow: hidden;
        background-size: cover;
        background: linear-gradient(to bottom, #274450b3 0%, #1c343ebf 100%, transparent), url({{url('/img/backgrd.png') }}) no-repeat;
      }
      .col-form-label {
        padding-left: 7px;
      }
     .error{
        color:red;
      }
      #loading-img {
        background: url(https://i1.wp.com/cdnjs.cloudflare.com/ajax/libs/galleriffic/2.0.1/css/loader.gif) center center no-repeat;
        height: 100%;
      }
      .loading {
        background: #a3a3a3;
        display: none;
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        opacity: 0.7;
        z-index: 20;
      }
      label.required:after {content: " *"; color: red;}
      .card_div{
        background-color: #274450;box-shadow: 0px 0px 10px 1px #888888;
      }
      .margin_top_m_15{
        margin-top:-15px;
      }
      .forgot_text{
        margin-left:41%;font-size: 24px; color:white
      }
      .margin_top_21{
        margin-top:21px;
      }
      .whitecolor{
        color:#fff;
      }
      #errorMsg{
        color:red;
        font-size:22px !important;
      }
      .forgot_btn{
        width:100%; height:40px;
      }
      @media screen and (min-width: 320px) and (max-width: 767px){
        .forgot_text {
          margin-left: 0%;
        }
        .margin_top_10{
          margin-top: 10px;
        }
        .margin_top_m_5{
          margin-top: -5px;
        }
        .padding_left_18{
          padding-left: 18px;
        }
      }
      @media screen and (min-width: 768px) and (max-width: 1023px){
        .card_div {
          width: 135%;
          margin-left: -21%;
        }
      }
    </style>
  </head>
  
  <body style="" class="app flex-row align-items-center">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-5">
          <div class="card p-4 card_div">
            <div class="card-body margin_top_m_15">
              <h1 class="forgot_text">{{ trans('messages.Forgot_Password') }}</h1>
              <form action="" method="POST" class="margin_top_21">
                @csrf
                <span id="errorMsg"></span>
                <div class="loading"><div id="loading-img"></div></div>
                <div class="form-group row">
                  <label for="email" class=" text_font col-md-3 whitecolor padding_left_18 col-form-label text-md-right required" required>{{ trans('messages.Email') }}</label>
                  <div class="col-md-9">
                    <input id="email" type="email" class=" text_font form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus data-required-message="Email is required." data-type-message="You must provide a valid email address.">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
                <div class="form-group row margin_top_m_5">
                  <div class="col-md-9 offset-md-3">
                    <div class="row">
                      <div class="col-sm-6"> <button class="text_font btn btn-primary forgot_btn px-4" onclick="sendResetLink()" type="button">{{ trans('messages.Send') }}</button></div>
                      <div class="col-sm-6"><a href="{{url('/login') }}"><button  class="text_font btn btn-primary forgot_btn margin_top_10 px-4" type="button">{{ trans('messages.Cancel') }}</button></a></div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
