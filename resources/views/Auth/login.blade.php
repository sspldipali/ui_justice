﻿﻿<!DOCTYPE html>
<html lang="en">
   <head>
   <?php App::setLocale('th');?>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>{{ trans('messages.Justice') }}</title>
    <link href="{!! asset('css/style.css') !!}" rel="stylesheet">
    <link href="{!! asset('vendors/css/allcss.css') !!}"rel="stylesheet">
    <link href="{!! asset('vendors/pace-progress/css/pace.min.css') !!}" rel="stylesheet">
    <script src="{!! asset('vendors/jquery/js/jquery.min.js') !!}" type="text/javascript"></script>
    <script type="text/javascript" src="{!! asset('vendors/js/jquery-2.1.4.js') !!}"></script>
    <script src="{!! asset('vendors/bootstrap/js/bootstrap.min.js') !!}"type="text/javascript"></script>
    <script type="text/javascript" src="{!! asset('vendors/js/globalize.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendors/js/dx.webappjs.js') !!}"></script>
    <script src="{!! asset('js/common_functions.js') !!}"></script>
    <script src="{!! asset('js/bootbox.min.js') !!}"></script>
    <script type="text/javascript">
      $(document).ready(function() {
      	$(".loading").hide();
   	  })
    
      $(document).ready(function () {
	      $("#btnSubmit").click(function() {
          if ($("#Name").val() == "")
    		  bootbox.alert(" {{ trans('messages.Enter_your_username') }}!", function() {});
          else if ($("#password").val() == "")
    		  bootbox.alert(" {{ trans('messages.Enter_your_password') }}!", function() {});
          else {
            $(".loading").hide();
            $("#loginID").submit(); // Submit the form
    	    }
        });
      });
    </script>
    <style>
      .btn-primary {
        color: #fff;
        background-color: #43768a;
        border-color: #43768a;
      }
      .text_colo_white{
        color:#fff;
      }
      .card {
        border: 1px solid #274450;
        border-radius: 0.25rem;
      }
      .text_align_c{
        text-align: center;
      }
      .card-body {
        padding: 1rem !important;
        margin-right: 21px;
      }
      .text_align_end{
        text-align: end;
      }
      .col-form-label {
       padding-left: 7px;
      }
      .error {
       color:  #E52421;
       font-size:22px !important;
       }
      #btnSubmit{
        width:100%;
        height: 100%;
      }
      #forget{
        width:100%;
      }
      #errorMsg{
        color:#E52421;
        font-size:22px !important;
      }
      label.required:after {content: " *"; color: #E52421;  font-size:22px !important;}
      #loading-img {
        background: url(https://i1.wp.com/cdnjs.cloudflare.com/ajax/libs/galleriffic/2.0.1/css/loader.gif) center center no-repeat;
        height: 100%;
      }
      .loading {
        background: #a3a3a3;
        display: none;
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        opacity: 0.7;
        z-index: 20;
      }
      .card_div{
        background-color: #274450;
      }
      .margin_bottom_20{
        margin-bottom:20px;
      }
      @media screen and (min-width: 768px) and (max-width:1023px){
        .card_div {
          background-color: #274450;
          width: 174%;
          margin-left: -38%;
        }
      }
      @media screen and (min-width: 1024px) and (max-width:1030px){
        .card_div {
          background-color: #274450;
          width: 128%;
          margin-left: -16%;
        }
      }
    </style>
  </head>

  <body style="" class="app flex-row align-items-center">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-5">
          <div class="card p-4 card_div">
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12">
                  <h1 class="text_colo_white text_align_c margin_bottom_20">{{ trans('messages.Login') }}</h1>
                </div>
              </div>
              <form method ="post" action="{{url('/login') }}" name="myform" id="loginID">
                <div class="loading"><div id="loading-img"></div></div>

                @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                @csrf
                <span id="errorMsg"></span>
                @if(isset($error_message) && $error_message != "")
                <div class="error">
                  <i class="fa fa-times-circle"></i>
                  <?php //echo $error_message; ?>
                  {{ $error_message }}
                </div>
                @elseif($exception_message)
                <div class="error">
                  <i class="fa fa-times-circle"></i>
                  <?php //echo $error_message; ?>
                  {{ $exception_message }}
                </div>
                @endif
                <div class="form-group row">
                  <label for="name" class="col-md-3 text_colo_white col-form-label text_font required" required>{{ trans('messages.Name') }}</label>
                  <div class="col-md-9">
                    <input id="Name" type="text" class=" text_font form-control  @error('name') is-invalid @enderror" name="Name" value="{{ old('name') }}"  autocomplete="email" autofocus>
                    @error('name')
                    <span class="invalid-feedback" role="alert" >
                      <strong style="color:red;">{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
                <div class="form-group row margin_top_m_10">
                  <label for="password" class="col-md-3  text_font text_colo_white col-form-label required" required>{{ trans('messages.Password') }}</label>
                  <div class="col-md-9">
                    <input id="password" type="password" class=" text_font form-control @error('password') is-invalid @enderror" name="password"  autocomplete="current-password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
                <div class="tile-footer">
                  <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                      <div class="row">
                        <div class="col-sm-6">
                          <button class="btn btn-primary text_font"  type="button"  id="btnSubmit">{{ trans('messages.Login') }}</button>
                        </div>
                        <div class="col-sm-6 margin_top_10">
                          <a href="{{url('/forgot_password') }}"><button class="btn btn-primary text_font" id="forget" type="button">{{ trans('messages.Forgot_Password') }}?</button></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
