@include('common.header')
@include('common.sidebar')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
<link href="{!! asset('css/error_log_page.css') !!}" rel="stylesheet">
<body class="app sidebar-fixed aside-menu-fixed sidebar-lg-show">
  <main class="main margin_top_66 margin_top_70">
    <header class="app-header navbar header_bk_co">
      <div class="whitecolor page_name">
        <h5 class="whitecolor"><a href="#" class="text_font22 whitecolor">{{ trans('messages.Report_An_Error') }}</a></h5>
      </div>
    </header>
    <div class="container-fluid margin_top_35">
      <div class="animated fadeIn">
        <div class="row ">
          <div class="col-sm-12">
            <h5 class="whitecolor display_flex font_size_15"><a href="{{ url('/home') }}" class="whitecolor">{{ trans('messages.Home') }}</a> /  {{ trans('messages.System_Administration') }} </h5>
          </div>
        </div>
        <div class="row margin_top_15 margin_top_0">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="border_cord">
                  <p class="col-sm-12 datadiv whitecolor" >{{ trans('messages.Search_Report') }}</p>
                  <div class="date_picker margin_top_2 serch_report_div">
                    <div class="form-group row">
                      <div class="col-md-1">
                        <label class="Since_label" for="example-date-input">{{ trans('messages.Since') }}</label>
                      </div>
                      <div class="col-md-3">
                        <input class="form-control" type="date" id="example-date-input">
                      </div>
                      <div class="col-md-1">
                        <label  for="example-date-input">{{ trans('messages.Until') }}</label>
                      </div>
                      <div class="col-md-3">
                        <input class="form-control" type="date" id="example-date-input">
                      </div>
                      <div class="col-md-3">
                        <button type="button" name="button" class="margin_top_2">{{ trans('messages.Searching') }}</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="border_cord">
                  <p class="col-sm-12 datadiv whitecolor">{{ trans('messages.Error_Report_List') }}</p>
                  <div class="col-sm-12 padding_left_right_10">
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered">
                        <thead>
                          <tr class="tr_bk_color">
                            <th>{{ trans('messages.Number') }}</th>
                            <th >{{ trans('messages.Date_Time') }}</th>
                            <th >{{ trans('messages.Menu') }}</th>
                            <th >{{ trans('messages.Error_Message') }}</th>
                            <th >{{ trans('messages.Username') }}</th>
                            <th >{{ trans('messages.Action') }}</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i = 0?>
                          @foreach($table as $key => $arr)
                          @foreach($arr as $userobj)
                          <?php $i++?>
                          <tr>
                            <td> {{ $i}}</td>
                            <td>{{ date('d/m/Y H:i:s', strtotime($userobj->created_at)) }}</td>
                            <td>{{ $userobj->action }}</td>
                            <td>{{ $userobj->exception }}</td>
                            <td>{{ $userobj->Name }}</td>
                            <td><a class="btn btn-danger margin_right_5" title="{{ trans('messages.Delete_Error_Report') }}"href="javascript:delete_error_log('{{$userobj->id}}')"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                          </tr>
                          @endforeach
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
<script src="{!! asset('vendors/popper.js/js/popper.min.js') !!}"type="text/javascript"></script>
<script src="{!! asset('vendors/bootstrap/js/bootstrap.min.js') !!}"type="text/javascript"></script>
<script src="{!! asset('vendors/pace-progress/js/pace.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') !!}"type="text/javascript"></script>
<script src="{!! asset('vendors/@coreui/coreui/js/coreui.min.js') !!}"type="text/javascript"></script>
<script>
$(document).ready(function() {
$('#example').DataTable();
} );
var url = '{{ Config::get('url.url') }}';
var token = '{{ Session::get('key') }}';
function delete_error_log(id) {
if(id != "") {
bootbox.confirm("{{ trans('messages.Delete_record_conformation') }} ?", function(r) {
if(r){
$.ajax({
type: "POST",
url: ""+url+"/delete_error_log/"+id+"",
contentType: "application/json; charset=utf-8",
dataType: "json",
success: function (data) {
bootbox.alert("{{ trans('messages.Error_report_delete_alert') }} ||", function(){
window.location = ""+baseURL+"/error_log";
});

},
failure: function (response) {
bootbox.alert("failed");
},
error: function (response) {
//alert("error");
}
});
}
});
}

}
</script>
</body>
@include('common.footer')
</html>