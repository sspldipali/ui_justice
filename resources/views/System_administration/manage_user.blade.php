@include('common.header')
@include('common.sidebar')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
<link href="{!! asset('css/user_manage_page.css') !!}" rel="stylesheet">
<body class="app sidebar-fixed aside-menu-fixed sidebar-lg-show">
  <main class="main margin_top_66 margin_top_70">
    <header class="app-header navbar header_bk_co">
      <div class="page_name whitecolor">
        <h5 class="whitecolor"><a href="#" class="text_font22 whitecolor">{{ trans('messages.User_Management') }}</a></h5>
      </div>
    </header>
    @if(isset($exception_message) && $exception_message != "")
    <span class="" style="color: red;">
      <i class="fa fa-times-circle"></i>
      <?php echo $exception_message; ?>
    </span>
    @endif
    <div class="container-fluid margin_top_35">
      <div class="animated fadeIn">
        <div class="row ">
          <div class="col-sm-12">
            <h5 class="whitecolor display_flex font_size_15"><a href="{{ url('/home') }}" class="whitecolor">{{ trans('messages.Home') }}</a> /  {{ trans('messages.System_Administration') }} </h5>
          </div>
        </div>
        <div class="row margin_top_15 margin_top_0">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="border_cord">
                  <p class="col-sm-12 datadiv whitecolor">{{ trans('messages.user_management_list') }}</p>
                  @if(isset($error_message) && $error_message != "")
                  <div class="error">
                    <i class="fa fa-times-circle"></i>
                    <?php //echo $error_message; ?>
                    {{ $error_message }}
                  </div>
                  @elseif(isset($exception_message) && $exception_message != "")
                  <div class="error">
                    <i class="fa fa-times-circle"></i>
                    <?php //echo $error_message; ?>
                    {{ $exception_message }}
                  </div>
                  @endif
                  <div class="col-sm-12 padding_left_right_10">
                  <div class="table-responsive">
                  <table id="example" class="table table-striped table-bordered">
                    <div class="row">
                      <div class="col-sm-12">
                        <button type="button" class="btn btn-primary margin_right_5" title="{{ trans('messages.Add_User') }}" style="float:right;" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i></button>
                      </div>
                    </div>
                    <thead>
                      <tr class="tr_bk_color">
                        <th >{{ trans('messages.Number') }}</th>
                        <th >{{ trans('messages.Name') }}</th>
                        <th >{{ trans('messages.Email') }}</th>
                        <th >{{ trans('messages.Role') }}</th>
                        <th >{{ trans('messages.Juridiction') }}</th>
                        <th >{{ trans('messages.Notification') }}</th>
                        <th >{{ trans('messages.Active') }}</th>
                        <th >{{ trans('messages.Action') }}</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i = 0?>
                      @foreach($table as $key => $arr)
                      @foreach($arr as $userobj)
                      <?php $i++?>
                      <tr>
                        <td> {{ $i}}</td>
                        <td>{{ $userobj->Name }}</td>
                        <td>{{ $userobj->Email }}</td>
                        <td>{{ $userobj->role_name }}</td>
                        <td>{{ $userobj->CourtName }}</td>
                        <td>
                          <div class="form-group form-check">
                            <label class="form-check-label">
                              @if($userobj->Notification == "1")
                                <input class="form-check-input" type="checkbox" name="remember" id="chkNotification" checked>
                              @else
                                <input class="form-check-input" type="checkbox" name="remember" id="chkNotification">
                              @endif
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-group form-check">
                            <label class="form-check-label">
                              @if($userobj->Active == "1")
                                <input class="form-check-input" type="checkbox" name="remember"id="chkActive" checked>
                              @else
                                <input class="form-check-input" type="checkbox" name="remember"id="chkActive">
                              @endif
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="btn-group">
                            <a class="btn btn-success margin_right_5"  title="{{ trans('messages.Reset_Password') }}" href="javascript:update_password_by_admin('{{$userobj->id}}')"><i class="fa fa-undo" aria-hidden="true"></i></a>
                            <a class="btn btn-primary margin_right_5" title="{{ trans('messages.Update_User') }}" href="javascript:get_user_by_id('{{$userobj->id}}')"><i class="fa fa-edit" aria-hidden="true"></i></a>
                            <a class="btn btn-danger margin_right_5"  title="{{ trans('messages.Delete_User') }}" href="javascript:delete_user('{{$userobj->id}}')"><i class="fa fa-trash" aria-hidden="true"></i></a>
                          </div>
                        </td>
                      </tr>
                      <div class="modal fade" id="myModal1" role="dialog" >
                        <div class="modal-dialog modal_dialog_new" style="left:0%;">
                          <div class="modal-content" style="width:112%;">
                            <div class="modal-header">
                              <h3>{{ trans('messages.User_Registration') }}</h3>
                              <button type="button"  class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                              <form id="adduserFormId">
							                  <div class="loading"><div id="loading-img"></div></div>
                                @csrf
								                <span id="errorMsg"></span>
                                <input type="hidden" class="form-control" id="juridiction" name="juridiction">
                                <div class="form-group dis_play" style="display: flex !important;" >
                                  <div class="col-sm-2">
                                    <label class="control-label required " for="name">{{ trans('messages.Name') }}:</label>
                                  </div>
                                  <div class="col-sm-10">
                                    <input type="text" class="form-control" id="Name" name="Name" required>
                                  </div>
                                </div>
                                <div class="form-group dis_play" style="display: flex !important;">
                                  <div class="col-sm-2">
                                    <label class="control-label required " for="email">{{ trans('messages.Email') }}:</label>
                                  </div>
                                  <div class="col-sm-10">
                                    <input type="email" class="form-control" id="Email" name="Email" required>
                                  </div>
                                </div>
                                <div class="form-group dis_play" style="display: flex !important;">
                                  <div class="col-sm-2">
                                    <label class="control-label " for="right">{{ trans('messages.Role') }}:</label>
                                  </div>
                                  <div class="col-sm-10">
                                    <select id="Role" name="Role" class="form-control"required>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group dis_play" style="display: flex !important;">
                                  <div class="col-sm-2">
                                    <label class="control-label" for="right">{{ trans('messages.Court') }}:</label>
                                  </div>
                                  <div class="col-sm-10">
                                    <select id="Court" name="Court" class="form-control">
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group" style="display: flex !important;">
                                  <div class="col-sm-3 ">
                                    <label class="control-label " for="right">{{ trans('messages.Notification') }}:</label>
                                  </div>
                                  <div class="col-sm-9 manage_chk">
                                    <div class="checkbox">
                                      <input type="checkbox" name="Notification" id ="Notification" required>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group" style="display: flex !important;">
                                  <div class="col-sm-3">
                                    <label class="control-label " for="right">{{ trans('messages.Active') }}:</label>
                                  </div>
                                  <div class="col-sm-9 manage_chk">
                                    <div class="checkbox">
                                      <input type="checkbox" name="Active"  id="Active" name="Active"checked>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group row ">
                                  <div class="col-md-4 offset-md-4">
                                    <button  class="btn btn-primary px-4"  title="{{ trans('messages.Add_User') }}"  type="button" style="border-radius: 6px;"id="btnSubmit" onclick="register()">
                                    {{ trans('messages.Add') }}
                                    </button>
                                    <a href="{{ url('/manage_user') }}"><button  class="btn btn-primary px-4" title="{{ trans('messages.Cancel') }}" type="button" style="    border-radius: 6px;margin-top: -35px;margin-left: 90px;">
                                    {{ trans('messages.Cancel') }}
                                    </button></a>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                      @endforeach
                      @endforeach
                      <div class="modal fade" id="myModal2" role="dialog" >
                        <div class="modal-dialog modal_dialog_new" style="left:0%;">
                          <div class="modal-content" style="width:112%;">
                            <div class="modal-header">
                              <h3>{{ trans('messages.Edit_User_Information') }}</h3>
                              <button type="button"  class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                              <form id="">
							                  <div class="loading"><div id="loading-img"></div></div>
                                @csrf
                                <span id="errorMsgUpdate"></span>
                                <span id="errorMsg"></span>
                                <input type="hidden" class="form-control" id="juridictionEdit" name="juridictionEdit">
                                <div class="form-group dis_play" style="display: flex !important;">
                                  <div class="col-sm-2">
                                    <label class="control-label required" for="name">{{ trans('messages.Name') }}:</label>
                                  </div>
                                  <div class="col-sm-10">
                                    <input type="text" class="form-control" id="NameEdit" name="NameEdit" required>
                                  </div>
                                </div>
                                <div class="form-group dis_play" style="display: flex !important;">
                                  <div class="col-sm-2">
                                    <label class="control-label required" for="email">{{ trans('messages.Email') }}:</label>
                                  </div>
                                  <div class="col-sm-10">
                                    <input type="email" class="form-control" id="EmailEdit" name="EmailEdit" required>
                                  </div>
                                </div>
                                <div class="form-group dis_play" style="display: flex !important;">
                                  <div class="col-sm-2">
                                    <label class="control-label " for="right">{{ trans('messages.Role') }}:</label>
                                  </div>
                                  <div class="col-sm-10">
                                    <select id="RoleEdit" name="RoleEdit" class="form-control"required>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group dis_play" style="display: flex !important;">
                                  <div class="col-sm-2">
                                    <label class="control-label" for="right">{{ trans('messages.Court') }}:</label>
                                  </div>
                                  <div class="col-sm-10">
                                    <select id="CourtEdit" name="CourtEdit" class="form-control"required>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group" style="display: flex !important;">
                                  <div class="col-sm-3">
                                    <label class="control-label " for="right">{{ trans('messages.Notification') }}:</label>
                                  </div>
                                  <div class="col-sm-9 manage_chk">
                                    <div class="checkbox">
                                      <input id ="NotificationEdit"  type="checkbox" name="NotificationEdit" required>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group" style="display: flex !important;">
                                  <div class="col-sm-3">
                                    <label class="control-label " for="right">{{ trans('messages.Active') }}:</label>
                                  </div>
                                  <div class="col-sm-9 manage_chk">
                                    <div class="checkbox">
                                      <input  id="ActiveEdit" type="checkbox"  name="ActiveEdit" required>
									                    <input type="hidden" id="editUserId" name="editUserId">
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group row ">
                                  <div class="col-md-2 offset-md-4">
                                    <button  class="btn btn-primary px-4" title="{{ trans('messages.Save') }}" type="button" style="border-radius: 6px;" onclick="update_user()">
                                      {{ trans('messages.Save') }}
                                    </button>
                                    <a href="{{ url('/manage_user') }}"><button  class="btn btn-primary px-4" title="{{ trans('messages.Cancel') }}" type="button" style="border-radius: 6px;margin-top: -35px;margin-left: 90px;">
                                      {{ trans('messages.Cancel') }}</button>
                                    </a>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </tbody>
                  </table>
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <!-- CoreUI and necessary plugins-->
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
  <script src="{!! asset('vendors/popper.js/js/popper.min.js') !!}"type="text/javascript"></script>
  <script src="{!! asset('vendors/bootstrap/js/bootstrap.min.js') !!}"type="text/javascript"></script>
  <script src="{!! asset('vendors/pace-progress/js/pace.min.js') !!}"type="text/javascript"></script>
  <script src="{!! asset('vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') !!}"type="text/javascript"></script>
  <script src="{!! asset('vendors/@coreui/coreui/js/coreui.min.js') !!}"type="text/javascript"></script>
  <script>
  //get court selected value fron lookup
  var court_text = $("#Court option:selected" ).text();
  $(document).ready(function() {
    $(".loading").hide();
  })
  //validation
  function validate_userform() {
  if ($("#Name").val() == "")
    		bootbox.alert("{{ trans('messages.Enter_Name') }}", function() {});
    	else if ($("#Email").val() == "")
    		bootbox.alert("{{ trans('messages.Enter_Email') }}", function() {});
    	else if ($("#Court").val() == "")
    		bootbox.alert("{{ trans('messages.Enter_Court') }}", function() {});
    	else {

    	}
  }
//edit form validation
//validation
function validate_usereditform() {
  if ($("#NameEdit").val() == "")
    		bootbox.alert("{{ trans('messages.Enter_Name') }}", function() {});
    	else if ($("#EmailEdit").val() == "")
    		bootbox.alert("{{ trans('messages.Enter_Email') }}", function() {});
    	else if ($("#CourtEdit").val() == "")
    		bootbox.alert("{{ trans('messages.Enter_Juridiction') }}", function() {});
    	else {
    	}
}
var url = '{{ Config::get('url.url') }}';
var token = '{{ Session::get('key') }}';
    $(document).ready(function() {
      $('#example').DataTable();
    } );
    
    function register() {
     
      validate_userform();
      var Name = document.getElementById('Name').value;
      var Email = document.getElementById('Email').value;
      var Role = document.getElementById('Role').value;
      var Court = document.getElementById('Court').value;
      var juridiction = document.getElementById('juridiction').value;
      var data = ({'Name':Name,'Email':Email,'Role':Role,'Court':Court,'juridiction':juridiction});
	  $(".loading").show();
      $.ajax({
        url: ""+url+"/register",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        data:JSON.stringify(data),
        //'{"id":"' + noteid+'"}',
        success: function (data) {
          $('.loading').hide();
         // bootbox.alert("{{ trans('messages.User_addedd_success') }}!!", function(){
            window.location = ""+baseURL+"/manage_user";
              //  });
                },
        failure: function (response) {
          alert("failed");
        },
		error: function (err, status, xhr) {
			   $(".loading").hide();
               console.log("Failure");
               console.log("response: ", err.responseJSON);
               $("#errorMsg").html(err.responseJSON.message);
        }
      });
    }
    function delete_user(id) {
      if(id != "") {
        bootbox.confirm("{{ trans('messages.Delete_user_conformation') }}", function(r) {
          if(r){
            $.ajax({
            type: "POST",
            url: ""+url+"/user/"+id+"",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
              success: function (data) {
                bootbox.alert("{{ trans('messages.User_delete_success') }}", function(){
                  window.location = ""+baseURL+"/manage_user";
                });
                },
              failure: function (response) {
                bootbox.alert("failed");
              },
              error: function (response) {
                alert("error");
              }
            });
          }
        });
      }
    }
    //update password by admin
    function update_password_by_admin(id) {
      if(id != "") {
        bootbox.confirm("{{ trans('messages.Update_password_by_admin') }}", function(r) {
          if(r){
      $.ajax({
        type: "POST",
        url: ""+url+"/update_password_by_admin/"+id+"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
          window.location = ""+baseURL+"/manage_user";
          //alert('success');
        },
        error: function (result) {
          alert("error");
              }
            });
          }
        });
      }
    }
   
/*This function is used to update the existing user.*/
function update_user() {
      //debugger;
      var id = document.getElementById("editUserId").value;
	  console.log("id===>"+id);
      var Name = document.getElementById('NameEdit').value;
      var Email = document.getElementById('EmailEdit').value;
      var Role = document.getElementById('RoleEdit').value;
      var Court = document.getElementById('CourtEdit').value;
      var Active = document.getElementById('ActiveEdit').value;
      var Notification = document.getElementById('NotificationEdit').value;
      var data = ({
        'Name': Name,
        'Email': Email,
        'Role': Role,
        'Court': Court,
        'Active': Active,
        'Notification': Notification
      });
      if ($("#NameEdit").val() == "") {
        bootbox.alert(" {{ trans('messages.Enter_Name') }}", function() {});
      } else if ($("#EmailEdit").val() == "") {
        bootbox.alert("{{ trans('messages.Enter_Email') }}", function() {});
      } else if ($("#CourtEdit").val() == "") {
        bootbox.alert("{{ trans('messages.Enter_Court') }}", function() {});
      } else {
        $(".loading").show();
        $.ajax({
          type: "POST",
          url: "" + url + "/update_user/" + id + "",
          data: JSON.stringify(data), //'{"id":"' + noteid+'"}',
          contentType: "application/json;",
          dataType: "json",
          success: function(data) {
            $('.loading').hide();
            //window.location = ""+app_link+"/manage_user";
            window.location = baseURL + "/manage_user";
          },
          complete: function() {
            $('#loading').hide();
          },
          failure: function(response) {
            //alert("failed");
            $('.loading').hide();
          },
          error: function(err, status, xhr) {
            console.log("Failure");
            console.log("response: ", err.responseJSON);
            $("#errorMsgUpdate").html(err.responseJSON.message);
            $(".loading").hide();
          }
        });
    }
}
    //get user by id for update user
    function get_user_by_id(id){
      $.ajax({
        type: "GET",
        url: ""+url+"/get_user_by_id/"+id+"",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
          var user_data=JSON.stringify(data);
          var result = JSON.parse(user_data);
          var orders1 = result.user;
          binddata(orders1);
          console.log(orders1[0].Email);
        },
        failure: function (response) {
          alert("failed");
        },
        error: function (response) {
          alert("error");
        }
      });
    }
    function binddata(orders1){
	  document.getElementById("editUserId").value = orders1[0].id;
      document.getElementById('NameEdit').value=orders1[0].Name;
      document.getElementById('EmailEdit').value=orders1[0].Email;
      document.getElementById('RoleEdit').value=orders1[0].Role;
      document.getElementById('CourtEdit').value=orders1[0].Court;
      document.getElementById('NotificationEdit').value=orders1[0].Notification;
      document.getElementById('ActiveEdit').value=orders1[0].Active;
	    var Notification = orders1[0].Notification;
      var Active = orders1[0].Active;
      //notification
      if(Notification == "1"){
      document.getElementById("NotificationEdit").checked = true;
      }
      else{
        document.getElementById("NotificationEdit").checked = false;
      }
      //active
      if(Active == "1"){
      document.getElementById("ActiveEdit").checked = true;
      }
      else{
        document.getElementById("ActiveEdit").checked = false;
      }
      $('#myModal2').modal('show');
    }
    //get user role
    $(document).ready(function () {
    $.ajax({
                type: "GET",
                url: ""+url+"/getUserRoles",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
               
                var user_data=JSON.stringify(data);
                var result = JSON.parse(user_data);
                var orders1 = result.user;
                roles(result.user);
                rolesEdit(result.user);
              // console.log(result.user[0]);
                },
                failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                    alert("error");
                }
            });
    });
//get add user roles drop down
    function roles(user){
                $.each(user, function (index, value) {
                    // APPEND OR INSERT DATA TO SELECT ELEMENT.
                    $('#Role').append('<option value="' + value.id + '">' + value.role_name + '</option>');
                });
     // alert(user);
    }
    //get edit user
    function rolesEdit(user){
    $.each(user, function (index, value) {
        // APPEND OR INSERT DATA TO SELECT ELEMENT.
        $('#RoleEdit').append('<option value="' + value.id + '">' + value.role_name + '</option>');
    });
    }
	//get court look up data
    $(document).ready(function () {
    $.ajax({
                type: "GET",
                url: ""+url+"/LookupCourt",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
               
                var user_data=JSON.stringify(data);
                var result = JSON.parse(user_data);
                var orders1 = result.user;
                court(result.user)
                courtEdit(result.user);
              // console.log(result.user[0]);
                },
                failure: function (response) {
                    alert("failed");
                },
                error: function (response) {
                    alert("error");
                }
            });
    });
	//append  court lookup for user register
    function court(user){
    $.each(user, function (index, value) {
        // APPEND OR INSERT DATA TO SELECT ELEMENT.
        $('#Court').append('<option value="' + value.Id + '">' + value.Jurisdiction + ' - '+ value.Code + ' '+ value.Name +'</option>');
    });
    }
	//append  court lookup for court edit
    function courtEdit(user){
    $.each(user, function (index, value) {
        // APPEND OR INSERT DATA TO SELECT ELEMENT.
        $('#CourtEdit').append('<option value="' + value.Id + '">' + value.Jurisdiction + ' - '+ value.Code + ' '+ value.Name +'</option>');
    });
    }

    	//append  court lookup for user juridiction
      function juridiction(user){
    $.each(user, function (index, value) {
        // APPEND OR INSERT DATA TO SELECT ELEMENT.
        $('#juridiction').append('<option value="' + value.Name + '">' + value.Jurisdiction + ' - '+ value.Code + ' '+ value.Name +'</option>');
    });
    }
  </script>
</body>
@include('common.footer')
</html>