<!DOCTYPE html>
<html lang="en">

<head>
  <?php App::setLocale('th'); ?>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <title>{{ trans('messages.Justice') }}</title>
  <!-- Icons-->
  <script src="{!! asset('vendors/jquery/js/jquery.min.js') !!}" type="text/javascript"></script>
  <link href="{!! asset('vendors/@coreui/icons/css/coreui-icons.min.css') !!}" rel="stylesheet">
  <link href="{!! asset('css/ThaiSansNeue-Regular.ttf') !!}">

  <link href="{!! asset('vendors/font-awesome/css/font-awesome.min.css')!!}" rel="stylesheet">
  <link href="{!! asset('vendors/simple-line-icons/css/simple-line-icons.css')!!}" rel="stylesheet">
  <!-- Main styles for this application-->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
  <link href="{!! asset('vendors/css/style.css') !!}" rel="stylesheet">
  <link href="{!! asset('vendors/css/allcss.css') !!}" rel="stylesheet">
  <link href="{!! asset('vendors/pace-progress/css/pace.min.css')!!}" rel="stylesheet">

  <link rel="stylesheet" href="https://unpkg.com/@coreui/coreui/dist/css/coreui.min.css">
  <link rel="icon" type="image/ico" href="./img/favicon.ico" sizes="any" />

  <!-- Main styles for this application-->
  <link href="css/style.css" rel="stylesheet">
  <link href="vendors/pace-progress/css/pace.min.css" rel="stylesheet">
  <!-- Global site tag (gtag.js) - Google Analytics-->
  <script type="text/javascript" src="{!! asset('vendors/js/jquery-2.1.4.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('vendors/js/globalize.min.js') !!}"></script>
  <script src='https://kit.fontawesome.com/a076d05399.js'></script>
  <script src="{!! asset('js/common_functions.js') !!}"></script>
  <script src="{!! asset('js/bootbox.min.js') !!}"></script>
  <script type="text/javascript">
    function preventBack() {
      window.history.forward();
    }
    setTimeout("preventBack()", 0);
    window.onunload = function() {
      null
    };
    var baseURL = "{{ url('/') }}";
    var token = "{{ Session::get('key') }}";
  </script>
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<header class="app-header navbar" style="background: #6599ad !important;">
    <div id="Justice_header" style="display: inline-flex; width: inherit;">
      <div class="top_header_text">
        <div class="row" style="display: flex; font-size: 25px;width:190%">
        <div class="col-sm-6">
          <a href="#" class="loginanduser_a">
            <i class='fas fa-user fa_user_icon'></i><span class="fa_user_text" style="    margin-top: -5px;margin-left: 22%">{{ Session::get('Name') }}</span>
          </a>
          </div>
          <div class="col-sm-6">
          <a href="{{ Config::get('url.app_link') }}logout" style="margin-top: -14px;">
            <button style="display:contents;">
              <img src="img/log_out_icon.png" style="margin-top: 26px;" class="log_out_img"><span class="fa_logout_text">{{ trans('messages.Logout') }}</span>
            </button></a>
            </div>
        </div>
      </div>
    </div>
  </header>
  <header class="app-header app-header1 navbar background_img d-none d-lg-block d-xl-block" style="margin-top:30px;height:109px !important;">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div id="Justice_header" style="display: inline-flex; width: inherit;">
      <a class="navbar-brand" href="#">
        <img class="navbar-brand-full logo_height_widt" src="img/1.png" title="{{ trans('messages.Justice') }}">
      </a>
      <div class="logoaftertext">
        <h4 style="font-size: 33px;">ศาลยุติธรรม</h4>
        <h6 style="font-size: 23px;">{{ trans('messages.Court_Of_justice') }}</h6>
      </div>
      <div class="WelCome_div" style="">
        <span class="WelCome_div_text" style="font-size: 29px;">{{ trans('messages.Justice_Dashboard') }}</span><br>
        <span style="font-size: 29px;">{{ trans('messages.Welcome_To_Dashboard') }}</span>
      </div>
    </div>
  </header>

  <header class="app-header app-header1 navbar background_img d-lg-none d-xl-none header_margin" style="margin-top:30px;height:113px !important;">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div id="Justice_header" style="display: inline-flex; width: inherit;">
      <div class="row width_100">
        <div class="col-sm-4">
          <a class="navbar-brand" href="#">
            <img class="navbar-brand-full logo_height_widt" src="img/1.png" title="{{ trans('messages.Justice') }}">
          </a>
        </div>
        <div class="col-sm-4">
          <div class="logoaftertext">
            <h4 style="color:#fff;font-size:16px;">ศาลยุติธรรม</h4>
            <h6 style="color:#fff;font-size:11px">{{ trans('messages.Court_Of_justice') }}</h6>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="WelCome_div">
            <span class="WelCome_div_text">{{ trans('messages.Justice_Dashboard') }}</span><br>
            <span class="WelCome_div_text_span">>{{ trans('messages.Welcome_To_Dashboard') }}</span>
          </div>
        </div>
      </div>
  </header>
</body>

</html>