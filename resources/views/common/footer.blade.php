</div>

<footer class="app-footer">


      <span class="text_font22"style="color:#fff;">{{ trans('messages.copyright') }}</span>

  </footer>
  <!-- CoreUI and necessary plugins-->
  <script src="{!! asset('vendors/popper.js/js/popper.min.js') !!}"
  type="text/javascript"></script>
  <script src="{!! asset('vendors/bootstrap/js/bootstrap.min.js') !!}"
  type="text/javascript"></script>
  <script src="{!! asset('vendors/pace-progress/js/pace.min.js') !!}"
  type="text/javascript"></script>
  <script src="{!! asset('vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') !!}"
  type="text/javascript"></script>
  <script src="{!! asset('vendors/@coreui/coreui/js/coreui.min.js') !!}"
  type="text/javascript"></script>

  </body>
</html>
