<?php App::setLocale('th'); ?>
    <div class="app-body">
      <div class="sidebar sidebar_div">
        <nav class="sidebar-nav">
          <ul class="nav">  
            <li class="nav-item">
              <a class="nav-link" href="{{url('/home') }}">
              <img src="{!! asset('img/home_icon.png') !!}" class="side_bar_icon_width">  {{ trans('messages.first_page') }}</a>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="{{url('/home') }}">
              <img src="{!! asset('img/report1_icon.png') !!}" class="side_bar_icon_width">  {{ trans('messages.Reports1') }}</a>
              <ul class="nav-dropdown-items">
              
              <li class="nav-item">
                  <a class="nav-link" href="{{url('/send_report') }}">
                  <i class="fa fa-envelope" aria-hidden="true"></i>  {{ trans('messages.Send_Report') }}</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('/delivery_history') }}">
                  <i class="fa fa-history" aria-hidden="true"></i>  {{ trans('messages.Delivery_History') }}</a>
                    <!-- <img src="/img/im.png" style="width: 17px;"> Delivery history</a> -->
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="{{url('/home') }}">
              <img src="{!! asset('img/report1_icon.png') !!}" class="side_bar_icon_width">  {{trans('messages.Reports2') }}</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item ">
                <a class="nav-link" ><i class="fa fa-balance-scale" aria-hidden="true"></i>  {{ trans('messages.Including_All_Court') }}</a>
                  
                </li>
                <li class="nav-item padding_left_20">
                  <a class="nav-link" href="{{url('/pie_in') }}">
                  <i class="fa fa-pie-chart" aria-hidden="true"></i>  {{ trans('messages.Pie_Graph') }}</a>
                </li>
                <li class="nav-item padding_left_20">
                  <a class="nav-link" href="{{url('/line_in') }}">
                  <i class="fa fa-line-chart" aria-hidden="true"></i>  {{ trans('messages.Line_Graph') }}</a>
                </li>
                <li class="nav-item padding_left_20">
                  <a class="nav-link" href="{{url('/bar_in') }}">
                  <i class="fa fa-compress" aria-hidden="true"></i>  {{ trans('messages.Comparison_Graph') }}</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" ><img src="{!! asset('img/within_the_court.png') !!}" class="side_bar_icon_width">  {{ trans('messages.Within_Court') }}</a>
                </li>
                <li class="nav-item padding_left_20">
                  <a class="nav-link" href="{{url('/pie_wi') }}">
                  <i class="fa fa-pie-chart" aria-hidden="true"></i>  {{ trans('messages.Pie_Graph') }}</a>
                </li>
                <li class="nav-item padding_left_20">
                  <a class="nav-link" href="{{url('/line_wi') }}">
                  <i class="fa fa-line-chart" aria-hidden="true"></i>  {{ trans('messages.Line_Graph') }}</a>
                </li>
                <li class="nav-item padding_left_20">
                  <a class="nav-link" href="{{url('/bar_wi') }}">
                  <i class="fa fa-compress" aria-hidden="true"></i>  {{ trans('messages.Comparison_Graph') }}</a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="{{url('/home') }}">
              <img src="{!! asset('img/gears icon.png') !!}" class="side_bar_icon_width">  {{ trans('messages.System_Administration') }}</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="{{url('/manage_user') }}">
                  <img src="{!! asset('img/username_icon.png') !!}" class="side_bar_icon_width">  {{ trans('messages.User_Management') }}</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('/access_report') }}">
                    <i class="nav-icon icon-puzzle"></i>  {{ trans('messages.Access_Report') }}</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('/error_log') }}">
                  <i class="fa fa-bug" aria-hidden="true"></i>  {{ trans('messages.Report_An_Error') }}</a>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
      </div>