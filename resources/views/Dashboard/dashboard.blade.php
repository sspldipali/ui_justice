@include('common.header')
@include('common.sidebar')
<body>
  <main class="main margin_top_66 margin_top_70">
  <header class="app-header navbar header_bk_co">
    <div class="page_name">
      <h5 class="whitecolor"><a href="#" class=" text_font22 whitecolor">{{ trans('messages.Home') }}</h5></a>
    <div>
  </header>
  <div class="container-fluid margin_top_35">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12">
        <h5 class="whitecolor display_flex font_size_15"><a href="#" class="whitecolor">{{ trans('messages.Home') }}</a> / {{ trans('messages.first_page') }} </h5>
        </div>
      </div>
    </div>
  </div>
  </main>
  @include('common.footer')
</body>
