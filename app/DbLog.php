<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DbLog extends Model
//class User extends Model implements Auditable

{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'action', 'exception',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

}
