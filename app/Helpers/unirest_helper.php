<?php

/**
 * @author Dipali Patil
 */

if (!function_exists("postWithJsonBody")) {
    function postWithJsonBody($endpoint, $postBody, $headers = array(
        'Accept' => 'application/json',
        'Content-Type' => 'application/json'
    ))
    {
        Log::info("Creating unirest request body...");
        $body = \Unirest\Request\Body::json($postBody);
        Log::info("Unirest request body created!");
        Log::info("Calling POST API: " . env("JUSTICE_BACKEND_URL") . $endpoint);
        $response = \Unirest\Request::post(env("JUSTICE_BACKEND_URL") . $endpoint, $headers, $body);
        return $response;
    }
}

/*if (! function_exists("callGetAPI")) {
    function callGetAPI($endpoint, $parameters = array(), $headers = array(
        'Accept' => 'application/json', 
        'Content-Type' => 'application/json')) {
        if (Session::exists("jwtToken")) {
            $headers["Authorization"] = "Bearer " . Session::get("jwtToken");
        }
        Log::info("Calling GET API: " . env("JUSTICE_BACKEND_URL") . $endpoint);
        $response = \Unirest\Request::get(env("JUSTICE_BACKEND_URL") . $endpoint, $headers, $parameters);
        return $response;
    }
}*/

if (!function_exists("postWithAttachments")) {
    function postWithAttachments($endpoint, $postBody, $filesArray, $headers = array(
        'Accept' => 'application/json',
        'Content-Type' => 'multipart/form-data'
    ))
    {
        Log::info("Creating unirest request body...");
        $body = \Unirest\Request\Body::multipart($postBody, $filesArray);
        Log::info(json_encode($body));
        Log::info("Unirest request body created!");
        Log::info("Calling POST API: " . env("JUSTICE_BACKEND_URL") . $endpoint);
        $response = \Unirest\Request::post(env("JUSTICE_BACKEND_URL") . $endpoint, $headers, $body);
        return $response;
    }
}
