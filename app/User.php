<?php

namespace App;

use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Contracts\Auditable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\Log;

class User extends Authenticatable implements JWTSubject, Auditable
//class User extends Model implements Auditable

{
    use \OwenIt\Auditing\Auditable;
    public function products()
    {
        return $this->hasMany(Product::class);
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Name', 'Email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * This method is used to get user by email id.
     */
    public function getUserByEmailId($EmailID)
    {
        $query = DB::table('users')
            ->where('email', $EmailID)
            ->select('users.*');
        $userDetails = $query->get();
        if (sizeof($userDetails) > 0) {
            return $userDetails[0];
        } else {
            return null;
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\MailResetPasswordNotification($token));
    }
}
