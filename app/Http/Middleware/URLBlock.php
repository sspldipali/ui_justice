<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class URLBlock
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * This method is used to block url without authentication.
         */
        if (Session::exists("loggedInUserId")) {
            return $next($request);
        } else {
            return redirect('/sessionexpired');
        }

    }
}
