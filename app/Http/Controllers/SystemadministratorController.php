<?php

namespace App\Http\Controllers;

use Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class SystemadministratorController extends JusticeController
{

    /**
     * This method is used to show the manage user view.
     */
    public function manage_user()
    {

        return view('System_administration.manage_user');
    }
    /**
     * This method is used to show the access report view.
     */
    public function access_report()
    {
        return view('System_administration.access_report');
    }
    /**
     * This method is used to show the error log view.
     */
    public function error_log()
    {
        return view('System_administration.error_log');
    }

    /**
     * This method is used to get the error log data
     */
    public function get_error_log()
    {
        $url = Config::get('url.url');
        $curl = curl_init();
        $token = Session::get('key');

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('JUSTICE_BACKEND_URL')."/api/get_error_log?token=$token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return redirect('/');
        } else {
            $respNode = json_decode($response);
            $data = array();
            $data['table'] = $respNode;
            return view('System_administration.error_log', $data);
        }

    }
    /**
     * This method is used to get access log data.
     */
    public function get_access_log()
    {

        $url = Config::get('url.url');
        $curl = curl_init();
        $token = Session::get('key');
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('JUSTICE_BACKEND_URL')."/api/get_access_log_data?token=$token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $respNode = json_decode($response);
            $data = array();
            $data['table'] = $respNode;
            return view('System_administration.access_report', $data);
        }
    }
    /**
     * This method is used to get the manage user data.
     */
    public function get_manage_user()
    {
        try {
            $token = Session::get('key');
            $url = Config::get('url.url');
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => env('JUSTICE_BACKEND_URL')."/api/get_User?token=$token",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    // Set Here Your Requesred Headers
                    'Content-Type: application/json',
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

            if ($err) {
                $this->data['error_message'] = "Something went wrong...";
                return View::make('System_administration.manage_user', $this->data);
            } else {
                $respNode = json_decode($response);
                $data = array();
                $data['table'] = $respNode;

                return view('System_administration.manage_user', $data);
            }
        } catch (\Exception $e) {
            $this->data['exception_message'] = "Something went wrong with manage user!";
            Log::info("Exception Occured===>" . $e->getMessage());
            return View::make('System_administration.manage_user', $this->data);
        }
    }

    /**
     * This method is used to get users role from roles table.
     */
    public function getUserRoles()
    {
        $url = Config::get('url.url');
        $curl = curl_init();
        $token = Session::get('key');
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('JUSTICE_BACKEND_URL')."/api/getUserRoles?token=$token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $respNode = json_decode($response);
            $roles = array();
            $roles['roles_table'] = $respNode;
            return view('System_administration.manage_user', $roles);
        }
    }
}
