<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class reportsController extends Controller
{
    /**
     * This method is used to show pie in charts for including court.
     */
    public function pie_in()
    {
        return view('reports.pie_in');
    }
    /**
     * This method is used to show line in charts for including court.
     */
    public function line_in()
    {
        return view('reports.line_in');
    }
    /**
     * This method is used to show bar in charts for including court.
     */
    public function bar_in()
    {
        return view('reports.bar_in');
    }

    /**
     * This method is used to show pie wi charts for within court.
     */
    public function pie_wi()
    {
        return view('reports.pie_wi');
    }
    /**
     * This method is used to show line wi charts for within court.
     */
    public function line_wi()
    {
        return view('reports.line_wi');
    }
    /**
     * This method is used to show bar wi charts for within court.
     */
    public function bar_wi()
    {
        return view('reports.bar_wi');
    }
    /**
     * This method is used to show send report view.
     */
    public function send_report()
    {
        return view('report.send_report');
    }
    /**
     * This method is used to show delivery history view.
     */
    public function delivery_history()
    {
        return view('report.delivery_history');
    }

    public function submitReport(Request $request)
    {
        try {
            Log::info("Submitted form: " . json_encode($request->all()));
            $reportData = array();
            //$filesArray = array();
            $fileCount = 0;
            foreach ($request->all() as $key => $value) {
                if (!$request->hasFile($key)) {
                    $reportData[$key] = $value;
                } else {
                    $fileCount += 1;
                    $file = $request->file($key);
                    $fileName = $file->getClientOriginalName();
                    $filePath = public_path('/temp_uploads/report_uploads/');
                    $file->move($filePath, $fileName);
                    //$filesArray[str_replace(".", '_', $fileName)] = $filePath;
                    $reportData['letterFilePath'] = $filePath . $fileName;
                }
            }
            $reportData['fileCount'] = $fileCount;
            $response = postWithJsonBody("/api/STSend", $reportData);
            if ($response != null && $response->code == 200) {
                $data = array(
                    "message" => $response->body->message,
                    "response" => $response->body
                );
                return view('report.send_report', $data);
            } else {
                return view('report.send_report', array('error_message' => $response->body->message));
            }
            //return view('report.send_report', array('message' => "Report submitted successfully!"));
        } catch (\Exception $e) {
            Log::info("Exception: " . $e->getMessage());
            Log::error($e);
            return view('report.send_report', array('message' => "Report submitted successfully!"));
        }
    }

    public function getReportPreview(Request $request)
    {
        Log::info("Reched reportsController@getReportPreview");
        $finYear = $request->finYear;
        $quarter = $request->quarter;
        $response = postWithJsonBody("/api/report/preview", array("finYear" => $finYear, "quarter" => $quarter), array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            "Authorization" => $request->header("Authorization")
        ));
        if ($response->code == 200) {
            return response()->json(array(
                "success" => true,
                "message" => "Preview data fetched successfully!",
                "data" => $response->body->data
            ), 200);
        } else {
            return response()->json(array(
                "success" => true,
                "message" => $response->body->message
            ), 400);
        }
    }
}
