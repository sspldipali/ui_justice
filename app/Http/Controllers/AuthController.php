<?php

namespace App\Http\Controllers;

use App\Http\Controllers\JusticeController;
use App\User;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class AuthController extends JusticeController
{


    public $loginAfterSignUp = true;
    //This method used to show reset password view
    public function reset_password($id)
    {
        return view('Auth.reset_password');
    }
    //This method used to show login view
    public function login_view()
    {
        return view('Auth.login', $this->data);
    }
    // this methos used to call login api using curl
    public function getAuthenticationToken(Request $request)
    {
        try {

            $user = new User();
            $Name = $user->Name = $request->input("Name");
            $password = $user->password = $request->input("password");
            $data1 = [
                'Name' => $Name,
                'password' => $password,
            ];
            $url = Config::get('url.url');
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => env('JUSTICE_BACKEND_URL') . "/api/login",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($data1),
                CURLOPT_HTTPHEADER => array(
                    // Set here requred headers
                    "accept: */*",
                    "accept-language: en-US,en;q=0.8",
                    "content-type: application/json",
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $data = json_decode($response);
            $Role = Session::get('Role');
            if ($Role == 4) {
                return redirect('/');
            } else if ($data->success == 1) {
                Session(['key' => $data->token]);
                Session(['Name' => $data->Name]);
                Session(['Role' => $data->Role]);
                Session(['user_id' => $data->id]);
                Session::put('loggedInUserId', $data->id);
                return redirect('/home');
            } else {
                $this->data['error_message'] = "ชื่อผู้ใช้และรหัสผ่านไม่ถูกต้อง";
                return View::make('Auth.login', $this->data);
            }
        } catch (\Exception $e) {
            //throw new \App\Exceptions\LogData($e);
            $this->data['exception_message'] = "มีบางอย่างผิดพลาดในการเข้าสู่ระบบ";
            Log::info("Exception Occured===>" . $e->getMessage());
            return View::make('Auth.login', $this->data);
        }
    }

    /**
     * This method is used to show the message for expired session.
     */
    public function sessionExpired()
    {

        $data['error_message'] = "เซสชันหมดอายุแล้ว!";
        return view("Auth.login", $data);
    }
    /**
     * This method used to show forgot password view
     */
    public function forgot_password()
    {
        return view('Auth.forgot_password');
    }

    /**
     * This method used for logout
     */
    public function get_logout()
    {
        Session::flush();
        $curl = curl_init();
        $token = Session::get('key');
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('JUSTICE_BACKEND_URL') . "/api/logout",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return redirect('/login');
        }
    }

    public function testSTReceive(Request $request)
    {
        try {
            Log::info("in testAPI : " . json_encode($request->all()));
            $responseJson = array();
            $responseData = array();
            foreach ($request->all() as $key => $value) {
                Log::info("Key => " . $key);
                if ($request->hasFile($key)) {
                    $file = $request->file($key);
                    /* //To save file to the storage/app folder
                        $file->store($key . ".txt");
                    */
                    Log::info("File name: " . $file->getClientOriginalName());
                    //To save file to the drive (Another folder other than project folder)
                    $filePath = 'F:/files_to_upload/files_uploaded/';
                    $fileName = $file->getClientOriginalName();
                    $file->move($filePath, $fileName);
                    $responseData['letterFilePath'] = $filePath . $fileName;
                } else {
                    $responseData[$key] = $value;
                }
                /*
                //Additional file methods:
                $file->getRealPath();
                $file->getClientOriginalName();
                $file->getClientOriginalExtension();
                $file->getSize();
                $file->getMimeType();
                */
            }
            $responseJson['message'] = "Report submitted successfully!";
            $responseJson['success'] = true;
            $responseJson['data'] = $responseData;
            //Log::info(json_encode($request->allFiles()));
            return response()->json($responseJson, 200, array("Content-Type" => "application/json", "Accept" => "application/json"));
        } catch (\Exception $e) {
            Log::info("Exception: " . $e->getMessage());
            Log::error($e);

            $exceptionJson = array(
                'message' => 'Something went wrong!',
                'success' => false,
                'exception' => $e->getMessage()
            );
            return response()->json(
                $exceptionJson,
                400,
                array("Content-Type" => "application/json", "Accept" => "application/json")
            );
        }
    }
}
