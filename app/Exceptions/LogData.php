<?php
 
namespace App\Exceptions;
 
use Exception;
use Auth;
use App\DbLog;
use App\User;
use Config;
use Illuminate\Support\Facades\Session;

class LogData extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report(){
    }
 
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */

   

    public function render($request, Exception $exception){
      $name = Session::get('Name');
      $log = new DbLog;
     // $id = 28;
     //$name1 = Auth::user()->Name;
      $log->username = $name;
      $log->action = $request->fullUrl();
      $log->exception = $exception;
      $log->save(); 
      return \Redirect::back()->with('error', 'Something Went Wrong.');
    }
}