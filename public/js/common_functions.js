$(document).ready(function() {
	$("#btnReset").click(function() {
		var resetURL = $(this).attr('resetURL');
		if (resetURL != "" && resetURL != undefined) {
			goToURL(resetURL);
		}
	});
});

function submitHTMLForm(formId, action, confirmMsg, doConfirm) {
	var myForm = document.getElementById(formId);
	
	if (action !== "") {
		myForm.action = action;
	}
	if (confirmMsg == null || confirmMsg === "") {
		confirmMsg = "Are you sure you want to submit the form?";
	}
	if (doConfirm) {
		bootbox.confirm(confirmMsg, function(r) {
			if (r) {
				myForm.submit();
			}
		});
	} else {
		myForm.submit();
	}
}

function goToURL(url) {
	window.location = url;
}

function addToSelectBox(selectBox, jsonObject) {
	for (var key in jsonObject) {
		var newOption = document.createElement('option');
		newOption.value = key;
		newOption.innerHTML = jsonObject[key];
		selectBox.appendChild(newOption);
	}
}

function addToSelectBoxWithDefaultSelectOption(selectBox, jsonObject) {
	
	var newOption = document.createElement('option');
	newOption.value = "";
	newOption.innerHTML = "---Select---";
	selectBox.appendChild(newOption);
	
	for (var key in jsonObject) {
		var newOption = document.createElement('option');
		newOption.value = key;
		newOption.innerHTML = jsonObject[key];
		selectBox.appendChild(newOption);
	}
}

function addToSelectBoxUsingJsonObjectArray(selectBox, jsonObjectArray) {
	for(var arrKey in jsonObjectArray) {
		var jsonObject = jsonObjectArray[arrKey];
		for (var key in jsonObject) {
			var newOption = document.createElement('option');
			newOption.value = key;
			newOption.innerHTML = jsonObject[key];
			selectBox.appendChild(newOption);
		}
	}
}

function removeAllSelectBoxOptions(selectBox) {
	while (selectBox.options.length > 0) {
		selectBox.remove(0);
	}
}

/**
 * 
 * @param requestURL
 * @param returnDataType
 * @param postDataObject
 * @param methodName
 * @param getParameterString
 */
function makeSyncAjaxRequest(requestURL, returnDataType, postDataObject, methodName, getParameterString) {
	var newRequestURL = requestURL;
	if (getParameterString != "")
		newRequestURL = newRequestURL+"/"+getParameterString;
	console.log("newRequestURL: " + newRequestURL);
	var returnObject = {};
	$.ajax({
		url: newRequestURL,
		type: methodName,
		data: postDataObject,
		dataType: returnDataType,
		async: false,
		success: function(result, textStatus, xhr) {
			returnObject['statusCode'] = xhr.status;
			returnObject['output'] = result;
		},
		error: function(xhr, status, error) {
			returnObject['statusCode'] = xhr.status;
			returnObject['statusText'] = xhr.statusText;
			returnObject['error'] = error;
			returnObject['respnseText'] = xhr.responseText;
		}
	});
	return returnObject;
}

function makeAjaxRequest(requestURL, returnDataType, dataObject, methodName, getParameterString, isAsynchronised) {
	var newRequestURL = requestURL;
	if (getParameterString != "")
		newRequestURL = newRequestURL+"/"+getParameterString;
	//var newRequestURL = requestURL+"/"+getParameterString;
	var returnObject = {};
	$.ajax({
		url: newRequestURL,
		type: methodName,
		data: dataObject,
		dataType: returnDataType,
		async: isAsynchronised,
		success: function(result, textStatus, xhr) {
			returnObject['statusCode'] = xhr.status;
			returnObject['output'] = result;
		},
		error: function(xhr, status, error) {
			returnObject['statusCode'] = xhr.status;
			returnObject['statusText'] = xhr.statusText;
			returnObject['error'] = error;
			returnObject['responseText'] = xhr.responseText;
		}
	});
	return returnObject;
}

function addJQueryDatePicker(elementId, minDate, maxDate, dateFormat, changeMonth, changeYear, yearRangeStart, yearRangeEnd) {
	var currentYear = new Date().getFullYear();
	var d = new Date(currentYear, 0, 1);
	var datePickerProperties = {};
	
	if (minDate !== "" && minDate !== null) {
		datePickerProperties["minDate"] = minDate;
	}
	
	if (maxDate !== "" && maxDate !== null) {
		datePickerProperties["maxDate"] = maxDate;
	}
	
	if (dateFormat !== "" && dateFormat !== null) {
		datePickerProperties["dateFormat"] = dateFormat;
	}
	
	if (changeMonth !== "" && changeMonth !== null) {
		datePickerProperties["changeMonth"] = changeMonth;
	}
	
	if (changeYear !== "" && changeYear !== null) {
		datePickerProperties["changeYear"] = changeYear;
	}
	
	if (yearRangeStart !== "" && yearRangeStart !== null) {
		var yRange = yearRangeStart;
		if (yearRangeEnd !== "" && yearRangeEnd !== null) {
			yRange = yRange + ":" + yearRangeEnd;
		}
		datePickerProperties["yearRange"] = yRange;
	}
	
	$("#"+elementId).datepicker(datePickerProperties);
}